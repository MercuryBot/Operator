## @package pyexample
#  MainWindow.py
#
#  @brief File containing the MainWindow class pattern
#  @author William Marcoux
#  @date   April 3rd, 2018
#  @version 1.0.0

import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from GlobalMemory import GlobalMemory
from SerialCom import SerialCom
from Logistic import Logistic

def get_action(widget, name, action_func, tip):
    action = QtWidgets.QAction(name, widget)
    if action_func:
        action.triggered.connect(action_func)
    action.setStatusTip(tip)
    return action

## The OperationButton class
#  @brief The OperationButton is a basic stylized configured QPushButton
class OperationButton(QtWidgets.QPushButton):

    ## The class' initialisation method
    #  @param self The object pointer
	#  @param parent to set the widget to
	#  @param size_x Size X of the button
	#  @param size_y Size y of the button
	#  @param img_rel Link to the image of the button when released
	#  @param img_pres Link to the image of the button when pressed
	#  @param img_dbled Link to the image of the button when disabled
    def __init__(self, parent=None, size_x=0, size_y=0, img_rel=None, img_pres=None, img_dbled=None, clicked_func=None):
        super(OperationButton, self).__init__(parent)
        self.setFixedWidth(size_x)
        self.setFixedHeight(size_y)
        self.img_rel = img_rel
        self.img_pres = img_pres
        self. img_dbled = img_dbled
        self.clicked_function = clicked_func
        self.setIcon(QtGui.QIcon(self.img_rel))
        self.setIconSize(QtCore.QSize(size_x, size_y))
        self.setStyleSheet('QPushButton{border: 0px solid;}')
        self.pressed.connect(lambda: self.press_event())
        self.released.connect(lambda: self.release_event())
        self.setMouseTracking(True)
		
	## Method to disable the button
    #  @param self The object pointer
    def disable(self):
        self.setIcon(QtGui.QIcon(self.img_dbled))
        self.pressed.disconnect()
        self.released.disconnect()

	## Method to disable the button
    #  @param self The object pointer
    def enable(self):
        self.pressed.connect(lambda: self.press_event())
        self.released.connect(lambda: self.release_event())
        self.setIcon(QtGui.QIcon(self.img_rel))
	
	## Setter of the function to associate on click action
    #  @param self The object pointer
	#  @param clicked_function Function to set
    def connect_func(self, clicked_function):
        self.clicked_function = clicked_function
        if self.clicked_function is None:
            self.clicked.disconnect()
			
	## Method to call the on-click function event
    #  @param self The object pointer
    def press_event(self):
        self.setIcon(QtGui.QIcon(self.img_pres))
        if self.clicked_function is not None:
            self.clicked_function()

	## Method to call the on-release function event
    #  @param self The object pointer
    def release_event(self):
        self.setIcon(QtGui.QIcon(self.img_rel))

	## Method to call the on-hover function qevent
    #  @param self The object pointer
	#  @param qevent Qt associated event
    def enterEvent(self, qevent):
        self.setStyleSheet('QPushButton{border: 5px solid darkgrey; border-radius: 55px;}')

	## Method to call the on-hover-out function qevent
    #  @param self The object pointer
	#  @param qevent Qt associated event
    def leaveEvent(self, qevent):
        self.setStyleSheet('QPushButton{border: 0px solid grey;}')

		
## The DebugConsole class
#  @brief The DebugConsole is an input enabled console for a give process
class DebugConsole(QtWidgets.QWidget):


    ## The class' initialisation method
    #  @param self The object pointer
	#  @param parent Parent widget to associte to set the widget to
	#  @param size_x Size X of the console
	#  @param size_y Size y of the console
	#  @param title Title of the console
	#  @param playPause Allow control over operation stopping/resuming
	#  @param color_background Console's background color
	#  @param color_text Console's font color
    def __init__(self, parent=None, size_x=0, size_y=0, title="", playPause=True, color_background='black', color_text='white'):
        super(DebugConsole, self).__init__(parent)
        self.setMinimumWidth(size_x)
        self.setMinimumHeight(size_y)
        self.playPause  = playPause
        self.size_x = size_x
        self.size_y = size_y
        self.color_background = color_background
        self.color_text = color_text
        self.title = title

        self.title_label = QtWidgets.QLabel(self)
        self.title_label.setText(self.title)
        self.title_label.move(5, 3)

        self.close_btn = OperationButton(self, 15, 15, 'x_btn.png', 'x_btn.png', 'x_btn.png', lambda: self.closeAction())
        self.close_btn.move(self.size_x - 19, 4)
        self.close_btn.setToolTip('Close console')

        self.console = QtWidgets.QPlainTextEdit(self)
        self.console.resize(self.size_x - 8, self.size_y - 54)
        self.console.move(4, 22)
        self.console.setReadOnly(True)
        self.console.setStyleSheet("QPlainTextEdit { color: " + self.color_text + "; background-color: " + self.color_background + "; }")

        self.insert_text = QtWidgets.QLineEdit(self)
        self.insert_text.resize(self.size_x - 80, 30)
        self.insert_text.move(4, self.size_y - 32)
        self.insert_text.returnPressed.connect(self.inputEntered)

        self.pause_btn = OperationButton(self, 30, 30, 'pause_btn_rel.png', 'pause_btn_pres.png', 'pause_btn_dbled.png', lambda: self.pauseAction())
        self.pause_btn.move(self.size_x - 70, self.size_y - 32)
        self.pause_btn.setToolTip('Pause activty')

        self.start_btn = OperationButton(self, 30, 30, 'start_btn_rel.png', 'start_btn_pres.png', 'start_btn_dbled.png', lambda: self.resumeAction())
        self.start_btn.move(self.size_x - 35, self.size_y - 32)
        self.start_btn.setToolTip('Resume activty')
        self.start_btn.disable()

        self.drawFraming()

        self.pause_func = None
        self.start_func = None
        self.close_func = None
        self.manual_input_func = None
        self.paused = False

	## Draws a framing around itself
    #  @param self The object pointer
    def drawFraming(self):
        self.top_line = QtWidgets.QFrame(self)
        self.top_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.top_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.top_line.setFixedWidth(self.size_x)
        self.top_line.setFixedHeight(4)
        self.top_line.setStyleSheet('QFrame{border: 4px solid;}')

        self.bottom_line = QtWidgets.QFrame(self)
        self.bottom_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.bottom_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.bottom_line.setFixedWidth(self.size_x)
        self.bottom_line.setFixedHeight(4)
        self.bottom_line.setStyleSheet('QFrame{border: 4px solid;}')
        self.bottom_line.move(0, self.size_y - 4)

        self.left_line = QtWidgets.QFrame(self)
        self.left_line.setFrameShape(QtWidgets.QFrame.VLine)
        self.left_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.left_line.setFixedHeight(self.size_y)
        self.left_line.setFixedWidth(4)
        self.left_line.setStyleSheet('QFrame{border: 4px solid;}')
        self.left_line.move(0, 0)

        self.right_line = QtWidgets.QFrame(self)
        self.right_line.setFrameShape(QtWidgets.QFrame.VLine)
        self.right_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.right_line.setFixedHeight(self.size_y)
        self.right_line.setFixedWidth(4)
        self.right_line.setStyleSheet('QFrame{border: 4px solid;}')
        self.right_line.move(self.size_x - 4, 0)

        self.title_line = QtWidgets.QFrame(self)
        self.title_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.title_line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.title_line.setFixedWidth(self.size_x)
        self.title_line.setFixedHeight(4)
        self.title_line.move(0, 18)
        self.title_line.setStyleSheet('QFrame{border: 4px solid;}')
	
	## Method to set the callback to when the console gets closed
    #  @param self The object pointer
	#  @param new_func New function to set
    def setCloseFunc(self, new_func):
        self.close_func = new_func

			
	## Method to set the callback to when the console gets opened
    #  @param self The object pointer
	#  @param new_func New function to set
    def setStartFunc(self, new_func):
        self.start_func = new_func

			
	## Method to set the callback to when the console gets paused
    #  @param self The object pointer
	#  @param new_func New function to set
    def setPauseFunc(self, new_func):
        self.pause_func = new_func

	## Method to set the callback to when the console has a manual input
    #  @param self The object pointer
	#  @param new_func New function to set
    def setManualInputFunc(self, newFunc):
        self.manual_input_func = newFunc

			
	## Method to clear the text inside the console
    #  @param self The object pointer
    def clearText(self):
        self.console.setPlainText("")
        self.insert_text.setText("")
		
	## Method to call the closing action
    #  @param self The object pointer
    def closeAction(self):
        if self.close_func is not None:
            self.close_func()
        self.hide()

	## Method to call the pausing action
    #  @param self The object pointer
    def pauseAction(self):
        self.insert_text.setDisabled(True)
        self.insert_text.setText("")
        self.start_btn.enable()
        self.pause_btn.disable()
        self.paused = True
        if self.pause_func is not None:
            self.pause_func()

	## Method to call the resuming action
    #  @param self The object pointer
    def resumeAction(self):
        self.insert_text.setDisabled(False)
        self.pause_btn.enable()
        self.start_btn.disable()
        self.paused = False
        if self.start_func is not None:
            self.start_func()

	## Method to input text into console
    #  @param self The object pointer
	#  @param text The text to put in
    def inputText(self, text):
        self.console.insertPlainText(text)
        self.console.verticalScrollBar().setValue(self.console.verticalScrollBar().maximum())

	## Method to execute once the input has been entered
    #  @param self The object pointer
    def inputEntered(self):
        if self.paused is False:
            entered_text = self.insert_text.text()
            self.insert_text.setText("")
            self.console.insertPlainText("\n>>> " + entered_text + " \n")
            self.console.verticalScrollBar().setValue(self.console.verticalScrollBar().maximum())
            if self.manual_input_func is not None:
                self.manual_input_func(entered_text)

## The FetcherTable class
#  @brief The FetcherTable is a specialized table to display connected units on network
class FetcherTable(QtWidgets.QWidget):

    ## The class' initialisation method
    #  @param self The object pointer
	#  @param parent Parent widget to associate the widget to
	#  @param size_x Size X of the button
	#  @param size_y Size y of the button
	#  @param title Title of the table
    def __init__(self, parent=None, size_x=0, size_y=0, title=""):
        super(FetcherTable, self).__init__(parent)
        self.setMinimumWidth(size_x)
        self.setMinimumHeight(size_y)
        self.size_x = size_x
        self.size_y = size_y
        self.title = title

        self.title_label = QtWidgets.QLabel(self)
        self.title_label.setText(self.title)
        self.title_label.move(5, 3)

        self.main_table = QtWidgets.QTableWidget(self)
        self.main_table.setColumnCount(3)
        self.main_table.setRowCount(0)
        self.main_table.setGeometry(QtCore.QRect(0, 22, self.size_x - 4, self.size_y - 26))
        self.main_table.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.main_table.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        self.main_table.horizontalHeader().setSectionResizeMode(2, QtWidgets.QHeaderView.ResizeToContents)
        self.main_table.palette().setBrush(QtGui.QPalette.Highlight, QtGui.QBrush(QtGui.QColor('white')))
        self.main_table.palette().setBrush(QtGui.QPalette.HighlightedText, QtGui.QBrush(QtGui.QColor('black')))
        self.main_table.verticalHeader().hide()
        self.main_table.horizontalHeader().hide()
        self.main_table.setShowGrid(False)

        self.main_table.setMouseTracking(True)
        self.main_table.cellEntered.connect(self.cellHover)

        self.drawFraming()

        self.current_hover = [0, 0]
        self.kick_func = None
        self.ctrl_func = None

	## Draws a framing around itself
    #  @param self The object pointer.
    def drawFraming(self):
        self.top_line = QtWidgets.QFrame(self)
        self.top_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.top_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.top_line.setFixedWidth(self.size_x)
        self.top_line.setFixedHeight(4)
        self.top_line.setStyleSheet('QFrame{border: 4px solid;}')

        self.bottom_line = QtWidgets.QFrame(self)
        self.bottom_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.bottom_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.bottom_line.setFixedWidth(self.size_x)
        self.bottom_line.setFixedHeight(4)
        self.bottom_line.setStyleSheet('QFrame{border: 4px solid;}')
        self.bottom_line.move(0, self.size_y - 4)

        self.left_line = QtWidgets.QFrame(self)
        self.left_line.setFrameShape(QtWidgets.QFrame.VLine)
        self.left_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.left_line.setFixedHeight(self.size_y)
        self.left_line.setFixedWidth(4)
        self.left_line.setStyleSheet('QFrame{border: 4px solid;}')
        self.left_line.move(0, 0)

        self.right_line = QtWidgets.QFrame(self)
        self.right_line.setFrameShape(QtWidgets.QFrame.VLine)
        self.right_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.right_line.setFixedHeight(self.size_y)
        self.right_line.setFixedWidth(4)
        self.right_line.setStyleSheet('QFrame{border: 4px solid;}')
        self.right_line.move(self.size_x - 4, 0)

        self.title_line = QtWidgets.QFrame(self)
        self.title_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.title_line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.title_line.setFixedWidth(self.size_x)
        self.title_line.setFixedHeight(4)
        self.title_line.move(0, 18)
        self.title_line.setStyleSheet('QFrame{border: 4px solid;}')
	
	## Method to add a connected unit in the table
    #  @param self The object pointer
	#  @param unit_id ID of the unit to add to the table
    def addFetcher(self, unit_id):
        rowPos = self.main_table.rowCount()

        for i in range (0, rowPos):
            if unit_id in self.main_table.item(i, 0).text():
                return

        self.main_table.insertRow(rowPos)

        newItem = QtWidgets.QTableWidgetItem("Unit - " + unit_id, 1)
        newItem.setFlags(QtCore.Qt.NoItemFlags)
        newItem.setForeground((QtGui.QBrush(QtGui.QColor('black'))))
        self.main_table.setItem(rowPos, 0, newItem)
        newItem = QtWidgets.QTableWidgetItem("")
        newItem.setFlags(QtCore.Qt.NoItemFlags)
        self.main_table.setItem(rowPos, 1, newItem)
        newItem = QtWidgets.QTableWidgetItem("")
        newItem.setFlags(QtCore.Qt.NoItemFlags)
        self.main_table.setItem(rowPos, 2, newItem)
        newBtn = QtWidgets.QPushButton(" X ", self.main_table)
        newBtn.setFixedSize(60, 30)
        newBtn.setStyleSheet("QPushButton[enabled=\"false\"]"
                             "{ color: white }"
                             "QPushButton"
                             "{ color: red;"
                             "  font-weight: bold; "
                             "  background-color: white; }")
        newBtn.setToolTip("Kick unit from current network")
        newBtn.setFlat(True)
        newBtn.clicked.connect(lambda: self.delFetcher(unit_id))
        self.main_table.setCellWidget(rowPos, 1, newBtn)
        newBtn = QtWidgets.QPushButton(" Ctrl ", self.main_table)
        newBtn.setFixedSize(60, 30)
        newBtn.setStyleSheet("QPushButton[enabled=\"false\"]"
                                                    "{ color: white }"
                                                    "QPushButton"
                                                    "{ color: blue }"
                                                    "{ background-color: white}")
        newBtn.setToolTip("Pilot device manually")
        newBtn.setFlat(True)
        newBtn.clicked.connect(lambda: self.ctrlAction(unit_id))
        self.main_table.setCellWidget(rowPos, 2, newBtn)

        #TODO: Find solution for buttons not initially hiding
        self.main_table.cellWidget(rowPos, 1).hide()
        self.main_table.cellWidget(rowPos, 2).hide()

	## Method to remove a connected unit from the table (and the network)
    #  @param self The object pointer
	#  @param unit_id ID of the unit to remove from the table
    def delFetcher(self, unit_id):
        self.kickAction(unit_id)
        for i in range (0, self.main_table.rowCount()):
            if unit_id in self.main_table.item(i, 0).text():
                self.main_table.removeRow(i)
                self.cellHover(i, 0)
                return
				
	## Setter for the function to drop a Unit from the network
    #  @param self The object pointer
	#  @param new_func Function to set
    def setKickFunc(self, new_func):
        self.kick_func = new_func

	## Setter for the function to take manual control of a Unit on the network
    #  @param self The object pointer
	#  @param new_func Function to set
    def setCtrlFunc(self, new_func):
        self.ctrl_func = new_func

	## Caller to the function to drop a Unit from the network
    #  @param self The object pointer
	#  @param unit_id ID of targeted unit
    def kickAction(self, unit_id):
        if self.kick_func is not None:
            self.kick_func(unit_id)
			
	## Caller to the function to take manual control of a Unit on the network
    #  @param self The object pointer
	#  @param unit_id ID of targeted unit
    def ctrlAction(self, unit_id):
        if self.ctrl_func is not None:
            self.ctrl_func(unit_id)

	## Method to animate a cell hovering effect
	#  @param self The object pointer
	#  @param row Hovered row
	#  @param column Hovered column
    def cellHover(self, row, column):
        if self.main_table.item(self.current_hover[0], 0) is None:
            return
        if self.current_hover != [row, column]:
            self.main_table.item(self.current_hover[0], 0).setBackground(QtGui.QBrush(QtGui.QColor('white')))
            #TODO: Put back hiding when figured out proper way to hide buttons on appearance
            self.main_table.cellWidget(self.current_hover[0], 1).hide()
            self.main_table.item(self.current_hover[0], 1).setBackground(QtGui.QBrush(QtGui.QColor('white')))
            self.main_table.cellWidget(self.current_hover[0], 2).hide()
            self.main_table.item(self.current_hover[0], 2).setBackground(QtGui.QBrush(QtGui.QColor('white')))
        self.main_table.item(row, 0).setBackground(QtGui.QBrush(QtGui.QColor('lightgrey')))
        self.main_table.cellWidget(row, 1).show()
        self.main_table.item(row, 1).setBackground(QtGui.QBrush(QtGui.QColor('lightgrey')))
        self.main_table.cellWidget(row, 2).show()
        self.main_table.item(row, 2).setBackground(QtGui.QBrush(QtGui.QColor('lightgrey')))
        self.current_hover = [row, column]

## The Ui_MainWindow class
#  @brief Main program's window
class Ui_MainWindow(QtWidgets.QMainWindow):

	## Confirmation if the window has already been launched
    __launched = False
	## Instance of global memory
    mem_data = GlobalMemory()

	## The class' initialisation method
    #  @param self The object pointer
    def __init__(self):
        super().__init__()
        self.com_port_monitor = None
        self.algorithm_monitor = None
        self.packet_monitor = None
        self.fetchTable = None

        self.threadpool = QtCore.QThreadPool()
        self.serial = SerialCom(0, "serial_mw", 350)
        self.serial.mem_data = self.mem_data
        self.serial.signals.packetIn.connect(self.updateComPortMonitor)

        self.logistic = Logistic()
        self.logistic.mem_data = self.mem_data
        #TODO: Add logistic thread signals

	## Method to handle forwarding of serial packet to com port monitor
    #  @param self The object pointer
	#  @param packet_str String of the packet to forward
    def updateComPortMonitor(self, packet_str):
        self.com_port_monitor.inputText(packet_str)

	## Method to setup the status bar
    #  @param self The object pointer
	#  @param MainWindow Targeted window
    def setupStatusBar(self, MainWindow):
        MainWindow.statusBar()
        MainWindow.menuBar().setNativeMenuBar(False)
        MainWindow.view_menu = MainWindow.menuBar().addMenu('&View')
        MainWindow.tools_menu = MainWindow.menuBar().addMenu('&Tools')
        MainWindow.com_menu = MainWindow.menuBar().addMenu('&COM')
        actions_list = [get_action(MainWindow,
                                   '&Algorithm process',
                                   lambda: self.algorithmMonitorAction(MainWindow),
                                   'Show/Hide algorithm monitoring'),
                        get_action(MainWindow,
                                   '&Communication port packets',
                                   lambda: self.comPortMonitorAction(MainWindow),
                                   'Show/Hide communication port packets'),
                        get_action(MainWindow,
                                   '&Telecommunication packets',
                                   lambda: self.packetMonitorAction(MainWindow),
                                   'Show/Hide telecommunication packets')]
        for i in actions_list:
            i.setCheckable(True)
            MainWindow.view_menu.addAction(i)
        actions_list.clear()

        if self.mem_data.data_exist('ui', 'telecom'):
            actions_list.append(get_action(MainWindow,
                                           '&Telecommunication packet tool',
                                           lambda: self.mem_data.get_data('ui', 'telecom').launch(),
                                           'Launch telecommunication packet tool'))
        if self.mem_data.data_exist('ui', 'comport'):
            actions_list.append(get_action(MainWindow,
                                           '&Communication port packet tool',
                                           lambda: self.mem_data.get_data('ui', 'comport').launch(),
                                           'Launch communication port packet tool'))
        if self.mem_data.data_exist('ui', 'redis'):
            actions_list.append(get_action(MainWindow,
                                           '&Redis database peeking',
                                           lambda: self.mem_data.get_data('ui', 'redis').launch(),
                                           'Launch Redis database peeking tool'))

        if len(actions_list) is 0:
            actions_list.append(get_action(MainWindow,
                                           '&No tools available',
                                           None,
                                           'No tools available'))
            actions_list[0].setDisabled(True)

        for i in actions_list:
            MainWindow.tools_menu.addAction(i)
        actions_list.clear()

        com_menu_ports = QtWidgets.QMenu('Ports:', MainWindow)
        if MainWindow.mem_data.data_exist('action', 'com_list') and MainWindow.mem_data.data_exist('action', 'connect_com'):
            com_list =  MainWindow.mem_data.get_data('action', 'com_list')()
            for i in com_list:
                com_menu_ports.addAction(get_action(MainWindow, "&" + str(i), lambda: MainWindow.mem_data.get_data('action', 'connect_com')(9600, str(i)), str(i)))
                if i is com_list[len(com_list) - 1]:
                    MainWindow.mem_data.get_data('action', 'connect_com')(9600, str(i))
        else:
            empty_action = get_action(MainWindow, '&No ports available', None, 'No ports available')
            empty_action.setDisabled(True)
            com_menu_ports.addAction(empty_action)

        MainWindow.com_menu.addMenu(com_menu_ports)
        MainWindow.com_menu.addAction(get_action(MainWindow, '&Sweep ports', MainWindow.resweep(MainWindow), 'Sweep ports for available receptor'))

	## Method to resweep the communication ports
    #  @param self The object pointer
	#  @param MainWindow Targeted window
    def resweep(self, MainWindow):
        com_menu_ports = QtWidgets.QMenu('Ports:', MainWindow)
        com_list = MainWindow.mem_data.get_data('action', 'com_list')()
        for i in com_list:
            com_menu_ports.addAction(get_action(MainWindow, "&" + str(i), lambda: MainWindow.mem_data.get_data('action', 'connect_com')(9600, str(i)), str(i)))
            if i is com_list[len(com_list) - 1]:
                MainWindow.mem_data.get_data('action', 'connect_com')(9600, str(i))
        else:
            empty_action = get_action(MainWindow, '&No ports available', None, 'No ports available')
            empty_action.setDisabled(True)
            com_menu_ports.addAction(empty_action)
	
	## Method to setup the fetcher table
    #  @param self The object pointer
	#  @param MainWindow Targeted window
    def setupFetcherTable(self, MainWindow):
        MainWindow.fetchTable = FetcherTable(MainWindow, 275, 300, 'Connected units on network')
        MainWindow.fetchTable.move(10, 30)
        if self.mem_data.data_exist('action', 'kick'):
            MainWindow.fetchTable.setKickFunc(lambda unit_id: self.mem_data('action', 'kick')(unit_id))
        if self.mem_data.data_exist('ui', 'ctrl'):
            MainWindow.fetchTable.setCtrlFunc(lambda unit_id: self.mem_data('ui', 'ctrl')(unit_id))

	## Method to setup the "Stop All" and "Resume all" buttons
    #  @param self The object pointer
	#  @param MainWindow Targeted window
    def setupStopResAllButtons(self, MainWindow):
        MainWindow.btn_start_all = OperationButton(MainWindow, 130, 130, 'start_btn_rel.png', 'start_btn_pres.png', 'start_btn_dbled.png', lambda: self.start_all_func())
        MainWindow.btn_start_all.move(315, 55)
        MainWindow.btn_start_all.setToolTip('Resume warehouse activity')
        MainWindow.btn_start_all.disable()
        MainWindow.btn_stop_all = OperationButton(MainWindow, 130, 130, 'stop_btn_rel.png', 'stop_btn_pres.png', 'stop_btn_dbled.png', lambda: self.stop_all_func())
        MainWindow.btn_stop_all.move(315, 200)
        MainWindow.btn_stop_all.setToolTip('Stop warehouse activity')

	## Method to call a stop of all mechanical activity on network
    #  @param self The object pointer
    def stop_all_func(self):
        self.btn_start_all.enable()
        self.btn_stop_all.disable()
        if self.mem_data.data_exist('operation', 'stop_all') is True:
            self.mem_data.get_data('operation', 'stop_all')()
	
	## Method to call a resuming of all mechanical activity on network
    #  @param self The object pointer
    def start_all_func(self):
        self.btn_start_all.disable()
        self.btn_stop_all.enable()
        if self.mem_data.data_exist('operation', 'start_all') is True:
            self.mem_data.get_data('operation', 'start_all')()

	## Method to setup the algorithm monitor
    #  @param self The object pointer
	#  @param MainWindow Targeted window
    def setupAlgorithmMonitor(self, MainWindow):
        MainWindow.algorithm_monitor = DebugConsole(MainWindow, 500, 300, "Algorithm monitor")
        MainWindow.algorithm_monitor.move(300, 30)
        MainWindow.algorithm_monitor.setCloseFunc(lambda: MainWindow.algorithmMonitorAction(MainWindow))
        if MainWindow.mem_data.data_exist('operation', 'pause_algorithm') is True:
            MainWindow.algorithm_monitor.setPauseFunc(MainWindow.mem_data('operation', 'pause_algorithm')())
        if MainWindow.mem_data.data_exist('operation', 'resume_algorithm') is True:
            MainWindow.algorithm_monitor.setStartFunc(MainWindow.mem_data('operation', 'resume_algorithm')())
        MainWindow.algorithm_monitor.hide()

	## Method to setup the communication port monitor
    #  @param self The object pointer
	#  @param MainWindow Targeted window
    def setupComPortMonitor(self, MainWindow):
        MainWindow.com_port_monitor = DebugConsole(MainWindow, 460, 300, "Com port monitor")
        MainWindow.com_port_monitor.move(10, 340)
        MainWindow.com_port_monitor.setCloseFunc(lambda: MainWindow.comPortMonitorAction(MainWindow))
        MainWindow.com_port_monitor.setPauseFunc(lambda: MainWindow.serial.set_paused(True))
        MainWindow.com_port_monitor.setStartFunc(lambda: MainWindow.serial.set_paused(False))
        MainWindow.com_port_monitor.setManualInputFunc(MainWindow.serial.send_str)
        MainWindow.com_port_monitor.hide()

	## Method to setup the telecommunication port monitor
    #  @param self The object pointer
	#  @param MainWindow Targeted window
    def setupTelecomMonitor(self, MainWindow):
        MainWindow.packet_monitor = DebugConsole(MainWindow, 460, 300, "Telecommuincation packets monitor")
        MainWindow.packet_monitor.move(10, 340)
        MainWindow.packet_monitor.setCloseFunc(lambda: MainWindow.packetMonitorAction(MainWindow))
        MainWindow.packet_monitor.setPauseFunc(lambda: MainWindow.serial.send_packet("SPPA", {"answer":"NO"}))
        MainWindow.packet_monitor.setStartFunc(lambda: MainWindow.serial.send_packet("SPPA", {"answer":"YES"}))
        MainWindow.packet_monitor.hide()

	## Setup fo the contents of the whole window
    #  @param self The object pointer
	#  @param window Window in which to setup the contents
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        #TODO: Need to create an error case where file is unavailable
        MainWindow.setWindowIcon(QtGui.QIcon("Mercury.png"))

        MainWindow.setupStatusBar(MainWindow)
        MainWindow.setupFetcherTable(MainWindow)
        MainWindow.setupStopResAllButtons(MainWindow)
        MainWindow.setupTelecomMonitor(MainWindow)
        MainWindow.setupComPortMonitor(MainWindow)
        MainWindow.setupAlgorithmMonitor(MainWindow)
        MainWindow.resize(500, 500)

        MainWindow.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        MainWindow.threadpool.start(MainWindow.serial)

        self.serial.mem_data = self.mem_data

	## Retranslate the window's contents if necessary
    #  @param self The object pointer
	#  @param window Window in which to retranslate the contents
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Operator - Home"))
	
	## Method for launching the window
    #  @param self The object pointer
	#  @param unit_address Address of the unit to control
    def launch(self):
        if self.__launched is False:
            self.__launched = True
            self.setupUi(self)
            self.show()
			
	## Method for quitting the window
    #  @param self The object pointer
	#  @param unit_address Address of the unit to control
    def quitUi(self, MainWindow):
        sys.exit()

	## Method to execute activity related to the algorithm monitor
    #  @param self The object pointer
	#  @param unit_address Address of the unit to control
    def algorithmMonitorAction(self, MainWindow):
        if MainWindow.algorithm_monitor is None:
            return

        if MainWindow.algorithm_monitor.isHidden():
            MainWindow.algorithm_monitor.show()
            MainWindow.btn_start_all.move(815, 55)
            MainWindow.btn_stop_all.move(815, 200)
            MainWindow.setFixedWidth(1000)
            MainWindow.view_menu.actions()[0].setChecked(True)
        else:
            MainWindow.algorithm_monitor.hide()
            MainWindow.btn_start_all.move(315, 55)
            MainWindow.btn_stop_all.move(315, 200)
            if MainWindow.packet_monitor is not None and MainWindow.com_port_monitor is not None:
                if MainWindow.packet_monitor.isHidden() or MainWindow.com_port_monitor.isHidden():
                    MainWindow.setFixedWidth(500)
            MainWindow.view_menu.actions()[0].setChecked(False)

	## Method to execute activity related to the communication port monitor
    #  @param self The object pointer
	#  @param unit_address Address of the unit to control
    def comPortMonitorAction(self, MainWindow):
        if MainWindow.com_port_monitor is None:
            return

        if MainWindow.com_port_monitor.isHidden():
            MainWindow.com_port_monitor.show()
            if MainWindow.packet_monitor is not None:
                if MainWindow.packet_monitor.isHidden() is False:
                    MainWindow.setFixedWidth(1000)
                    MainWindow.packet_monitor.move(500, 340)
            MainWindow.view_menu.actions()[1].setChecked(True)
            MainWindow.setFixedHeight(715)
        else:
            MainWindow.com_port_monitor.hide()
            if MainWindow.packet_monitor is not None and MainWindow.algorithm_monitor is not None:
                if MainWindow.packet_monitor.isHidden() and MainWindow.algorithm_monitor.isHidden():
                    MainWindow.setFixedWidth(500)
            if MainWindow.packet_monitor is not None:
                if MainWindow.packet_monitor.isHidden():
                    MainWindow.setFixedHeight(400)
                else:
                    MainWindow.packet_monitor.move(10, 340)
                    MainWindow.setFixedWidth(500)
            MainWindow.view_menu.actions()[1].setChecked(False)

	## Method to execute activity related to the telecommunication monitor
    #  @param self The object pointer
	#  @param unit_address Address of the unit to control
    def packetMonitorAction(self, MainWindow):
        if MainWindow.packet_monitor is None:
            return

        if MainWindow.packet_monitor.isHidden():
            MainWindow.serial.send_packet("SPPA", {"answer":"YES"})
            MainWindow.packet_monitor.show()
            if MainWindow.com_port_monitor is not None:
                if MainWindow.com_port_monitor.isHidden() is False:
                    MainWindow.setFixedWidth(1000)
                    MainWindow.packet_monitor.move(500, 340)
            MainWindow.view_menu.actions()[2].setChecked(True)
            MainWindow.setFixedHeight(715)
        else:
            MainWindow.packet_monitor.hide()
            MainWindow.serial.send_packet("SPPA", {"answer": "NO"})
            if MainWindow.com_port_monitor is not None and MainWindow.algorithm_monitor is not None:
                if MainWindow.com_port_monitor.isHidden() and MainWindow.algorithm_monitor.isHidden():
                    MainWindow.setFixedWidth(500)
            if MainWindow.com_port_monitor is not None:
                if MainWindow.com_port_monitor.isHidden():
                    MainWindow.setFixedHeight(400)
            MainWindow.view_menu.actions()[2].setChecked(False)

