## @package pyexample
#  ControlWindow.py
#
#  @brief File containing the Ui_ControlWindow class pattern
#  @author William Marcoux
#  @date   April 23rd, 2018
#  @version 1.0.0

from PyQt5 import QtCore, QtGui, QtWidgets
from GlobalMemory import GlobalMemory

## The OperationButton class
#  @brief The OperationButton is a basic stylized configured QPushButton
class OperationButton(QtWidgets.QPushButton):

    ## The class' initialisation method
    #  @param self The object pointer
	#  @param parent to set the widget to
	#  @param size_x Size X of the button
	#  @param size_y Size y of the button
	#  @param img_rel Link to the image of the button when released
	#  @param img_pres Link to the image of the button when pressed
	#  @param img_dbled Link to the image of the button when disabled
    def __init__(self, parent=None, size_x=0, size_y=0, img_rel=None, img_pres=None, img_dbled=None, clicked_func=None):
        super(OperationButton, self).__init__(parent)
        self.setFixedWidth(size_x)
        self.setFixedHeight(size_y)
        self.img_rel = img_rel
        self.img_pres = img_pres
        self. img_dbled = img_dbled
        self.clicked_function = clicked_func
        self.setIcon(QtGui.QIcon(self.img_rel))
        self.setIconSize(QtCore.QSize(size_x, size_y))
        self.setStyleSheet('QPushButton{border: 0px solid;}')
        self.pressed.connect(lambda: self.press_event())
        self.released.connect(lambda: self.release_event())
        self.setMouseTracking(True)
	
	## Method to disable the button
    #  @param self The object pointer
    def disable(self):
        self.setIcon(QtGui.QIcon(self.img_dbled))
        self.pressed.disconnect()
        self.released.disconnect()
		
	## Method to disable the button
    #  @param self The object pointer
    def enable(self):
        self.pressed.connect(lambda: self.press_event())
        self.released.connect(lambda: self.release_event())
        self.setIcon(QtGui.QIcon(self.img_rel))
	
	## Setter of the function to associate on click action
    #  @param self The object pointer
	#  @param clicked_function Function to set
    def connect_func(self, clicked_function):
        self.clicked_function = clicked_function
        if self.clicked_function is None:
            self.clicked.disconnect()
	
	## Method to call the on-click function event
    #  @param self The object pointer
    def press_event(self):
        self.setIcon(QtGui.QIcon(self.img_pres))
        if self.clicked_function is not None:
            self.clicked_function()
			
	## Method to call the on-release function event
    #  @param self The object pointer
    def release_event(self):
        self.setIcon(QtGui.QIcon(self.img_rel))

	## Method to call the on-hover function qevent
    #  @param self The object pointer
	#  @param qevent Qt associated event
    def enterEvent(self, qevent):
        self.setStyleSheet('QPushButton{border: 5px solid darkgrey; border-radius: 55px;}')

	## Method to call the on-hover-out function qevent
    #  @param self The object pointer
	#  @param qevent Qt associated event
    def leaveEvent(self, qevent):
        self.setStyleSheet('QPushButton{border: 0px solid grey;}')

## The Ui_ControlWindow class
#  @brief Gives manual control over a unit
class Ui_ControlWindow(QtWidgets.QWidget):
	
	## Confirmation if the window has already been launched
    __launched = False

	## The class' initialisation method
    #  @param self The object pointer
    def __init__(self):
        super().__init__()

        self.help_dialog = None
        self.help_btn = None

        self.unit_address = None

        self.keys_dict = {'up' : False,
                          'down' : False,
                          'left' : False,
                          'right' : False,
                          'q' : False,
                          'w' : False,
                          'a' : False,
                          's' : False,
                          'd' : False}

        self.mem_data = GlobalMemory()

	## Setup for the displayed unit'S ID label
    #  @param self The object pointer
	#  @param window Window in which to setup
    def setupUnitLabel(self, window):
        if window.unit_address is None:
            return

        window.controlled_unit_lbl = QtWidgets.QLabel(window)
        window.controlled_unit_lbl.setText("Currently controlled unit: ")
        window.controlled_unit_lbl.move(10, 15)

        window.unit_id_lbl = QtWidgets.QLabel(window)
        window.unit_id_lbl.setText(window.unit_address)
        window.unit_id_lbl.move(190, 15)

	## Setup for the displayed help dialog button
    #  @param self The object pointer
	#  @param window Window in which to setup
    def setupHelp(self, window):
        window.help_dialog = QtWidgets.QDialog()
        window.help_dialog.setWindowTitle("Manual unit control - Help")
        window.help_dialog.setFixedHeight(200)
        window.help_dialog.setFixedWidth(500)
        window.ok_help_btn = QtWidgets.QPushButton("OK", window.help_dialog)
        window.ok_help_btn.clicked.connect(lambda : window.help_dialog.hide())
        window.ok_help_btn.move(220, 170)
        window.help_dialog_txt = QtWidgets.QPlainTextEdit(window.help_dialog)
        window.help_dialog_txt.setReadOnly(True)
        window.help_dialog_txt.resize(480, 130)
        window.help_dialog_txt.move(10, 10)
        window.help_dialog_txt.setPlainText(" U/H/J/K : Move the wheels \n"
                                            " W/S     : Move the X lift up and down \n"
                                            " A/D     : Deploy/Retract the mat \n"
                                            " Q       : Spin the mat \n")
        window.help_dialog_txt.setStyleSheet("QPlainTextEdit { color: black; background-color: lightgrey; }")
        window.help_dialog.hide()

        window.help_btn = OperationButton(window, 30, 30, 'help_btn_rel.png', 'help_btn_pres.png', 'help_btn_dbled.png', lambda : window.help_dialog.show())
        window.help_btn.move(560, 10)
        window.help_btn.setToolTip("Help")

	## Setup for the displayed unit's movements
    #  @param self The object pointer
	#  @param window Window in which to setup
    def setupMovementDisplay(self, window):
        window.wheels_display = QtWidgets.QPushButton(window)
        window.wheels_display.setIcon(QtGui.QIcon('direct_cc.png'))
        window.wheels_display.setIconSize(QtCore.QSize(200, 200))
        window.wheels_display.setStyleSheet('QPushButton{border: 0px solid;}')
        window.wheels_display.resize(200, 200)
        window.wheels_display.move(10, 300)
        window.wheels_display.show()

        window.lift_display = QtWidgets.QPushButton(window)
        window.lift_display.setIcon(QtGui.QIcon('lift_c.png'))
        window.lift_display.setIconSize(QtCore.QSize(300, 300))
        window.lift_display.setStyleSheet('QPushButton{border: 0px solid;}')
        window.lift_display.resize(300, 300)
        window.lift_display.move(300, 200)
        window.lift_display.show()

        window.rod_display = QtWidgets.QPushButton(window)
        window.rod_display.setIcon(QtGui.QIcon('rod_c.png'))
        window.rod_display.setIconSize(QtCore.QSize(300, 300))
        window.rod_display.setStyleSheet('QPushButton{border: 0px solid;}')
        window.rod_display.resize(300, 300)
        window.rod_display.move(10, 10)
        window.rod_display.show()

        window.mat_display = QtWidgets.QPushButton(window)
        window.mat_display.setIcon(QtGui.QIcon('mat_idle.png'))
        window.mat_display.setIconSize(QtCore.QSize(200, 200))
        window.mat_display.setStyleSheet('QPushButton{border: 0px solid;}')
        window.mat_display.resize(200, 200)
        window.mat_display.move(350, 50)
        window.mat_display.show()

	## Retranslate the window's contents if necessary
    #  @param self The object pointer
	#  @param window Window in which to retranslate the contents
    def retranslateUi(self, window):
        _translate = QtCore.QCoreApplication.translate
        window.setWindowTitle(_translate("ControlWindow", "Operator - Unit manual control tool"))
	
	## Setup fo the contents of the whole window
    #  @param self The object pointer
	#  @param window Window in which to setup the contents
    def setupUi(self, window):
        window.setObjectName("RedisWindow")
        # TODO: Need to create an error case where file is unavailable
        window.setWindowIcon(QtGui.QIcon("Mercury.png"))
        window.setFixedHeight(600)
        window.setFixedWidth(600)

        window.setupUnitLabel(window)
        window.setupHelp(window)
        window.setupMovementDisplay(window)

        window.retranslateUi(window)
		
	## Method for launching the window
    #  @param self The object pointer
	#  @param unit_address Address of the unit to control
    def launch(self, unit_address="Unknown"):
        if self.__launched is False:
            self.__launched = True
            self.unit_address = unit_address
            if self.mem_data.data_exist('action', 'send_com_packet'):
                self.mem_data.get_data('action', 'send_com_packet')("mode", {"address":unit_address, "mode":"manual"})
            self.setupUi(self)
            self.show()

	## Method to accept a close event
    #  @param self The object pointer
	#  @param event The event to close
    def closeEvent(self, event):
        self.unit_id = None
        self.__launched = False
        self.help_dialog.hide()
        if self.mem_data.data_exist('action', 'send_com_packet'):
            self.mem_data.get_data('action', 'send_com_packet')("MODE", {"address": self.unit_address, "mode": "auto"})
        event.accept()
		
	## Method update the controlled unit's movements
    #  @param self The object pointer
	#  @param event The event to close
	#  @param part Unit's mechanical part to update
    def updateMovement(self, window, part):
        if part is 'wheels':
            direction_matrix = [[["75", "100"],  ["100", "100"],    ["100", "75"]],
                                [["100",  "0"],  [ "0",   "0" ],    ["0",  "100"]],
                                [["-75", "-100"],["-100", "-100"] , ["-100", "-75"]]]
            matrix_image     = [['direct_ul.png', 'direct_uc.png', 'direct_ur.png'],
                                ['direct_cl.png', 'direct_cc.png', 'direct_cr.png'],
                                ['direct_dl.png', 'direct_dc.png', 'direct_dr.png']]
            index_v = 1
            index_h = 1

            if self.keys_dict['up'] is True and self.keys_dict['down'] is False:
                index_v -= 1
            elif self.keys_dict['down'] is True and self.keys_dict['up'] is False:
                index_v += 1
            if self.keys_dict['left'] is True and self.keys_dict['right'] is False:
                index_h -= 1
            elif self.keys_dict['right'] is True and self.keys_dict['left'] is False:
                index_h += 1
            matrix_vector = direction_matrix[index_v][index_h]
            window.wheels_display.setIcon((QtGui.QIcon(matrix_image[index_v][index_h])))
            if self.mem_data.data_exist('action', 'send_com_packet'):
                self.mem_data.get_data('action', 'send_com_packet')("MOVE", {"address": self.unit_address, "moveset": "L" + matrix_vector[0] +  "R" + matrix_vector[1]})
        elif part is 'mat':
            if self.mem_data.data_exist('action', 'send_com_packet'):
                if self.keys_dict['q'] is True:
                    window.mat_display.setIcon(QtGui.QIcon('mat_spinning.png'))
                    self.mem_data.get_data('action', 'send_com_packet')("MOVE", {"address": self.unit_address, "moveset": "M100"})
                else:
                    window.mat_display.setIcon(QtGui.QIcon('mat_idle.png'))
                    self.mem_data.get_data('action', 'send_com_packet')("MOVE", {"address": self.unit_address, "moveset": "M0"})
        elif part is 'rod':
                if self.keys_dict['a'] is True and self.keys_dict['d'] is False:
                    if self.mem_data.data_exist('action', 'send_com_packet'):
                        self.mem_data.get_data('action', 'send_com_packet')("MOVE", {"address": self.unit_address, "moveset": "R100"})
                    window.rod_display.setIcon(QtGui.QIcon('rod_l.png'))
                elif self.keys_dict['d'] is True and self.keys_dict['a'] is False:
                    if self.mem_data.data_exist('action', 'send_com_packet'):
                        self.mem_data.get_data('action', 'send_com_packet')("MOVE", {"address": self.unit_address, "moveset": "R-100"})
                    window.rod_display.setIcon(QtGui.QIcon('rod_r.png'))
                else:
                    if self.mem_data.data_exist('action', 'send_com_packet'):
                        self.mem_data.get_data('action', 'send_com_packet')("MOVE", {"address": self.unit_address, "moveset": "R0"})
                    window.rod_display.setIcon(QtGui.QIcon('rod_c.png'))
        elif part is 'lift':
            if self.keys_dict['w'] is True and self.keys_dict['s'] is False:
                if self.mem_data.data_exist('action', 'send_com_packet'):
                    self.mem_data.get_data('action', 'send_com_packet')("MOVE", {"address": self.unit_address, "moveset": "X100"})
                window.lift_display.setIcon(QtGui.QIcon('lift_u.png'))
            elif self.keys_dict['s'] is True and self.keys_dict['w'] is False:
                if self.mem_data.data_exist('action', 'send_com_packet'):
                    self.mem_data.get_data('action', 'send_com_packet')("MOVE", {"address": self.unit_address, "moveset": "X-100"})
                window.lift_display.setIcon(QtGui.QIcon('lift_d.png'))
            else:
                if self.mem_data.data_exist('action', 'send_com_packet'):
                    self.mem_data.get_data('action', 'send_com_packet')("MOVE", {"address": self.unit_address, "moveset": "X0"})
                window.lift_display.setIcon(QtGui.QIcon('lift_c.png'))

	## Method to handle key press events
    #  @param self The object pointer
	#  @param event The event to take in
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Q:
            self.keys_dict['q'] = True
            self.updateMovement(self, 'mat')
        elif event.key() == QtCore.Qt.Key_W:
            self.keys_dict['w'] = True
            self.updateMovement(self, 'lift')
        elif event.key() == QtCore.Qt.Key_A:
            self.keys_dict['a'] = True
            self.updateMovement(self, 'rod')
        elif event.key() == QtCore.Qt.Key_S:
            self.keys_dict['s'] = True
            self.updateMovement(self, 'lift')
        elif event.key() == QtCore.Qt.Key_D:
            self.keys_dict['d'] = True
            self.updateMovement(self, 'rod')
        elif event.key() == QtCore.Qt.Key_U:
            self.keys_dict['up'] = True
            self.updateMovement(self, 'wheels')
        elif event.key() == QtCore.Qt.Key_J:
            self.keys_dict['down'] = True
            self.updateMovement(self, 'wheels')
        elif event.key() == QtCore.Qt.Key_H:
            self.keys_dict['left'] = True
            self.updateMovement(self, 'wheels')
        elif event.key() == QtCore.Qt.Key_K:
            self.keys_dict['right'] = True
            self.updateMovement(self, 'wheels')

	## Method to handle key release events
    #  @param self The object pointer
	#  @param event The event to take in
    def keyReleaseEvent(self, event):
        if event.key() == QtCore.Qt.Key_Q:
            self.keys_dict['q'] = False
            self.updateMovement(self, 'mat')
        elif event.key() == QtCore.Qt.Key_W:
            self.keys_dict['w'] = False
            self.updateMovement(self, 'lift')
        elif event.key() == QtCore.Qt.Key_A:
            self.keys_dict['a'] = False
            self.updateMovement(self, 'rod')
        elif event.key() == QtCore.Qt.Key_S:
            self.keys_dict['s'] = False
            self.updateMovement(self, 'lift')
        elif event.key() == QtCore.Qt.Key_D:
            self.keys_dict['d'] = False
            self.updateMovement(self, 'rod')
        elif event.key() == QtCore.Qt.Key_U:
            self.keys_dict['up'] = False
            self.updateMovement(self, 'wheels')
        elif event.key() == QtCore.Qt.Key_J:
            self.keys_dict['down'] = False
            self.updateMovement(self, 'wheels')
        elif event.key() == QtCore.Qt.Key_H:
            self.keys_dict['left'] = False
            self.updateMovement(self, 'wheels')
        elif event.key() == QtCore.Qt.Key_K:
            self.keys_dict['right'] = False
            self.updateMovement(self, 'wheels')