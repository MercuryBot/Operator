## @package pyexample
#  TimedSleep.py
#
#  @brief File containing the timed sleep functions
#  @author William Marcoux
#  @date   April 2nd, 2018
#  @version 1.0.0

from time import sleep

def usleep(us):
    sleep(us/1000000.0)


def msleep(ms):
    sleep(ms/1000.0)
