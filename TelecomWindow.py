## @package pyexample
#  TelecomWindow.py
#
#  @brief File containing the Ui_TelecomWindow class pattern
#  @author William Marcoux
#  @date   April 15th, 2018
#  @version 1.0.0

from PyQt5 import QtCore, QtGui, QtWidgets
from GlobalMemory import GlobalMemory
import string

## The Ui_ComportWindow class
#  @brief Widget meant to artifically send a telecommunication packet
class Ui_TelecomWindow(QtWidgets.QWidget):

    ## The class' initialisation method
    #  @param self The object pointer
    def __init__(self):
        super().__init__()
        self.target_address_field = None
        self.self_address_field = None
        self.self_address_default_chkbx = None
        self.head_field = None
        self.body_field = None
        self.hex_dropdown = None
        self.hex_insert_btn = None
        self.hex_delete_btn = None
        self.hex_body_view = None
        self.full_packet_view = None
        self.send_packet_btn = None
        self.__launched = False
        self.mem_data = GlobalMemory()
        self.body_buffer = []

				
    ## Setup for the address-related fields
    #  @param self The object pointer
	#  @param window Window in which to setup
    def setupAddressFields(self, window):
        window.target_address_field_lbl = QtWidgets.QLabel(window)
        window.target_address_field_lbl.setText("Target address: ")
        window.target_address_field_lbl.move(10, 15)

        window.target_address_field = QtWidgets.QLineEdit(window)
        window.target_address_field.setMaxLength(5)
        window.target_address_field.resize(300, 30)
        window.target_address_field.move(140, 10)
        window.target_address_field.textEdited.connect(lambda: window.addHexCharacterFromLineEdit(window))

        window.self_address_field_lbl = QtWidgets.QLabel(window)
        window.self_address_field_lbl.setText("Sender's address: ")
        window.self_address_field_lbl.move(10, 55)

        window.self_address_field = QtWidgets.QLineEdit(window)
        window.self_address_field.setMaxLength(5)
        window.self_address_field.resize(300, 30)
        window.self_address_field.move(140, 50)
        window.self_address_field.textEdited.connect(lambda: window.addHexCharacterFromLineEdit(window))

        window.self_address_default_chbx_lbl = QtWidgets.QLabel(window)
        window.self_address_default_chbx_lbl.setText("Use Receptor's address")

        window.self_address_default_chbx_lbl.move(10, 450)
        window.self_address_default_chkbx = QtWidgets.QCheckBox(window)
        window.self_address_default_chkbx.move(175, 453)
        window.self_address_default_chkbx.clicked.connect(lambda: window.useDefaultSelfAddress(window))

	## Setup for the packet-related fields
    #  @param self The object pointer
	#  @param window Window in which to setup
    def setupPacketFields(self, window):
        window.head_field_lbl = QtWidgets.QLabel(window)
        window.head_field_lbl.setText("Packet's head: ")
        window.head_field_lbl.move(10, 120)

        window.head_field = QtWidgets.QLineEdit(window)
        window.head_field.setMaxLength(4)
        window.head_field.resize(300, 30)
        window.head_field.move(140, 115)
        window.head_field.textEdited.connect(lambda: window.addHexCharacterFromLineEdit(window))

        window.body_field_lbl = QtWidgets.QLabel(window)
        window.body_field_lbl.setText("Packet's body: ")
        window.body_field_lbl.move(10, 160)

        window.body_field = QtWidgets.QLineEdit(window)
        window.body_field.setMaxLength(16)
        window.body_field.resize(300, 30)
        window.body_field.move(140, 155)
        window.body_field.textEdited.connect(lambda: window.addHexCharacterFromLineEdit(window))

	## Setup for the hexadecimal input utilities
    #  @param self The object pointer
	#  @param window Window in which to setup
    def setupHexUtils(self, window):
        window.hex_dropdown_lbl = QtWidgets.QLabel(window)
        window.hex_dropdown_lbl.setText("Hex code to add: ")
        window.hex_dropdown_lbl.move(10, 200)

        window.hex_dropdown = QtWidgets.QComboBox(window)
        window.hex_dropdown.move(150, 195)
        for i in range(0, 256):
            window.hex_dropdown.addItem(hex(i))

        window.hex_insert_btn = QtWidgets.QPushButton(window)
        window.hex_insert_btn.setText("Add")
        window.hex_insert_btn.setToolTip("Add hex character to body")
        window.hex_insert_btn.move(265, 195)
        window.hex_insert_btn.clicked.connect(lambda: window.addHexCharacter(window, window.hex_dropdown.currentIndex()))

        window.hex_delete_btn = QtWidgets.QPushButton(window)
        window.hex_delete_btn.setText("Delete")
        window.hex_delete_btn.setToolTip("Delete last character from body")
        window.hex_delete_btn.move(360, 195)
        window.hex_delete_btn.clicked.connect(lambda: window.delHexCharacter(window))

        window.hex_body_view_lbl = QtWidgets.QLabel(window)
        window.hex_body_view_lbl.setText("Hexadecimal view of current body:")
        window.hex_body_view_lbl.move(10, 230)
        window.hex_body_view = QtWidgets.QPlainTextEdit(window)
        window.hex_body_view.setReadOnly(True)
        window.hex_body_view.resize(430, 80)
        window.hex_body_view.move(10, 250)
        window.hex_body_view.setStyleSheet("QPlainTextEdit { color: green; background-color: lightgrey; }")

	## Setup for the sending utilities
    #  @param self The object pointer
	#  @param window Window in which to setup
    def setupSendUtils(self, window):
        window.full_packet_view_lbl = QtWidgets.QLabel(window)
        window.full_packet_view_lbl.setText("Packet preview: ")
        window.full_packet_view_lbl.move(10, 340)

        window.full_packet_view = QtWidgets.QPlainTextEdit(window)
        window.full_packet_view.setReadOnly(True)
        window.full_packet_view.resize(430, 50)
        window.full_packet_view.move(10, 360)
        window.full_packet_view.setStyleSheet("QPlainTextEdit { color: green; background-color: lightgrey; }")

        window.send_packet_btn = QtWidgets.QPushButton(window)
        window.send_packet_btn.setText("Send")
        window.send_packet_btn.setToolTip("Send packet")
        window.send_packet_btn.move(360, 450)
        window.send_packet_btn.setDisabled(True)
        window.send_packet_btn.clicked.connect(lambda: window.sendData(window))

	## Setup fo the contents of the whole window
    #  @param self The object pointer
	#  @param window Window in which to setup the contents
    def setupUi(self, window):
        window.setObjectName("TelecomWindow")
        #TODO: Need to create an error case where file is unavailable
        window.setWindowIcon(QtGui.QIcon("Mercury.png"))
        window.setFixedHeight(500)
        window.setFixedWidth(500)

        window.setupAddressFields(window)
        window.setupPacketFields(window)
        window.setupHexUtils(window)
        window.setupSendUtils(window)

        window.updateFullPacketView(window, window.head_field.text(), window.body_buffer)

        window.retranslateUi(window)

	## Retranslate the window's contents if necessary
    #  @param self The object pointer
	#  @param window Window in which to retranslate the contents
    def retranslateUi(self, window):
        _translate = QtCore.QCoreApplication.translate
        window.setWindowTitle(_translate("TelecomWindow", "Operator - Telecommunication packet tool"))
	
	## Method for launching the window
    #  @param self The object pointe
    def launch(self):
        if self.__launched is False:
            self.__launched = True
            self.setupUi(self)
            self.show()
			
	## Method to accept a close event
    #  @param self The object pointer
	#  @param event The event to close
    def closeEvent(self, event):
        self.__launched = False
        event.accept()

	## Method to set sending address to receptor's address
    #  @param self The object pointer
	#  @param window Window in which to execute the operation
    def useDefaultSelfAddress(self, window):
        if window.self_address_field is None:
            return
        if window.self_address_default_chkbx.isChecked():
            window.self_address_field.setDisabled(True)
            if window.mem_data.data_exist('info', 'self_address'):
                window.self_address_field.setText(window.mem_data.get_data('info', 'self_address'))
            else:
                window.self_address_field.setText("XXXXX")
        else:
            window.self_address_field.setDisabled(False)

	## Method to add hexadecimal characters to payload from the line edit
    #  @param self The object pointer
	#  @param window Window in which to execute the operation
    def addHexCharacterFromLineEdit(self, window):
        window.body_buffer.clear()
        for i in range(0, len(window.body_field.text())):
            window.body_buffer.append(ord(window.body_field.text()[i]))

        window.updateFullPacketView(window, window.head_field.text(), window.body_buffer)

	## Method to add hexadecimal characters to payload
    #  @param self The object pointer
	#  @param window Window in which to execute the operation
	#  @param character Character value to add
    def addHexCharacter(self, window, character):
        if len(window.body_buffer) < 16:
            window.body_buffer.append(character)
            if character < 34 or character is 127:
                window.body_field.setDisabled(True)
        window.updateFullPacketView(window, window.head_field.text(), window.body_buffer)

	## Method to delete an hexadecimal character from payload
    #  @param self The object pointer
	#  @param window Window in which to execute the operation
    def delHexCharacter(self, window):
        isTextEditable = True
        if len(window.body_buffer) > 0:
            del window.body_buffer[-1]
            for i in window.body_buffer:
                if i < 34 or i is 127:
                    isTextEditable = False
            if isTextEditable or len(window.body_buffer) is 0:
                window.body_field.setDisabled(False)
            window.updateFullPacketView(window, window.head_field.text(), window.body_buffer)

	## Method to send currently set packet over the telecommunication port
    #  @param self The object pointer
	#  @param window Window in which to execute the operation
    def sendData(self, window):
        packet_to_send = ""
        body_buffer_str = ''.join(chr(v) for v in window.body_buffer)
        packet_to_send += '<' + window.target_address_field.text() + window.self_address_field.text() + window.head_field.text() + body_buffer_str
        for i in range(len(window.body_buffer), 15):
            packet_to_send += 0
        packet_to_send += '>'

        if window.mem_data.data_exist('action', 'send_com_packet'):
            window.mem_data.get_data('action', 'send_com_packet')("PCKT", {"packet":packet_to_send,"address":window.target_address_field.text()})

	## Gette of a string corresponding to a hex string form of the currently set packet
    #  @param self The object pointer
	#  @param body_list List of the body's bytes
    def hexBodyViewStr(self, body_list):
        packet_str = ""

        for i in range(0, len(body_list)):
            packet_str += hex(body_list[i]) + " "

        return packet_str

	## Method to update the full packet view
    #  @param self The object pointer
	#  @param window Window in which to execute the operation
	#  @param head Head of the packet
	#  @param body_list List of the body's bytes
    def updateFullPacketView(self, window, head, body_list):
        packet_str = "<" + window.target_address_field.text() + window.self_address_field.text() + head

        for i in body_list:
            if i < 34 or i is 127:
                packet_str += "\\x" + str(i)
            else:
                packet_str += chr(i)

        for i in range(len(body_list), 15):
            packet_str += "\\x0"

        packet_str += ">"

        if len(window.target_address_field.text()) < 5 or len(window.self_address_field.text()) < 5 or len(head) < 4:
            window.full_packet_view.setStyleSheet("QPlainTextEdit { color: red; background-color: lightgrey; }")
            window. send_packet_btn.setDisabled(True)
        else:
            window.full_packet_view.setStyleSheet("QPlainTextEdit { color: green; background-color: lightgrey; }")
            window. send_packet_btn.setDisabled(False)
        window.hex_body_view.setPlainText(window.hexBodyViewStr(body_list))

        window.full_packet_view.setPlainText(packet_str)