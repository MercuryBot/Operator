## @package pyexample
#  TThread.py
#
#  @brief File containing the TThread class pattern
#  @author William Marcoux
#  @date   April 2nd, 2018
#  @version 1.0.0

from threading import Thread
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from TimedSleep import usleep

## The TMutex class
#  @brief Basic thread template for multi-threading in Python
class TThread(QRunnable):

    exit_flag = 0
    sleep_time_us = 0

	## The class' initialisation method
    #  @param self The object pointer
	#  @param thread_id Thread ID to set
	#  @param name Name of the thread
	#  @param sleep_time_us Sleep delay on task's periodic routine
    def __init__(self, thread_id=0, name="lambda", sleep_time_us=1):
        QRunnable.__init__(self)
        self.thread_id = thread_id
        self.name = name
        self.sleep_time_us = sleep_time_us
		
	## Main routine function of the thread
    #  @param self The object pointer
    @pyqtSlot()
    def run(self):
        while self.exit_flag is 0:
            self.task()
            usleep(self.sleep_time_us)

	## Task to execute in the main routine function of the thread (has to be set later on)
    #  @param self The object pointer
    def task(self):
        pass

	## Method for stopping the thread activity
    #  @param self The object pointer
    def stop(self):
        self.exit_flag = 1

