## @package pyexample
#  TMutex.py
#
#  @brief File containing the TMutex class pattern
#  @author William Marcoux
#  @date   April 2nd, 2018
#  @version 1.0.0

from threading import Lock
from Borg import Borg

## The TMutex class
#  @brief Class illustrating a mutex for python multi-threading operations
class TMutex(Borg):
	
	## Main mutex lock
    main_lock = Lock()
	## Dictionnary of mutex locks
    lock_dict = {}

	## The class' initialisation method
    #  @param self The object pointer
	#  @param *args List of locks to add on initialization
    def __init__(self, *args):
        self.main_lock.acquire()
        super(TMutex, self).__init__()
        for i in args:
            if isinstance(i, str):
                self.lock_dict[i] = Lock()
        self.main_lock.release()

	## Add a mutex lock to the mutex lock dictionnary
    #  @param self The object pointer
	#  @param *args List of locks to add
    def add_lock(self, *args):
        self.main_lock.acquire()
        for i in args:
            if isinstance(i, str):
                self.lock_dict[i] = Lock()
        self.main_lock.release()

	## Remove mutex locks from the mutex lock dictionnary
    #  @param self The object pointer
	#  @param *args List of locks to remove
    def remove_lock(self, *args):
        self.main_lock.acquire()
        for i in args:
            if isinstance(i, str):
                if self.lock_dict[i].locked():
                    self.lock_dict[i].release()
                del self.lock_dict[i]
        self.main_lock.release()

	## Engage lock of certain mutex locks
    #  @param self The object pointer
 	#  @param *args List of locks to lock
    def lock(self, *args):
        for i in args:
            if isinstance(i, str) and i in self.lock_dict.keys():
                self.lock_dict[i].acquire()

	## Disengage lock of certain mutex locks
    #  @param self The object pointer
 	#  @param *args List of locks to unlock
    def unlock(self, *args):
        for i in args:
            if isinstance(i, str) and i in self.lock_dict.keys():
                self.lock_dict[i].release()
