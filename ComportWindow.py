## @package pyexample
#  ComportWindow.py
#
#  @brief File containing the Ui_ComportWindow class pattern
#  @author William Marcoux
#  @date   April 12nd, 2018
#  @version 1.0.0

from PyQt5 import QtCore, QtGui, QtWidgets
from GlobalMemory import GlobalMemory
import json
from collections import OrderedDict

## The ContentsTable class
#  @brief Widget meant to display a set of hex character
class ContentsTable(QtWidgets.QWidget):
	
	## The class' initialisation method.
    #  @param self The object pointer.
    def __init__(self, parent=None, size_x=0, size_y=0, title=""):
        super(ContentsTable, self).__init__(parent)
        self.setMinimumWidth(size_x)
        self.setMinimumHeight(size_y)
        self.size_x = size_x
        self.size_y = size_y
        self.title = title

        self.title_label = QtWidgets.QLabel(self)
        self.title_label.setText(self.title)
        self.title_label.move(5, 3)

        self.main_table = QtWidgets.QTableWidget(self)
        self.main_table.setColumnCount(2)
        self.main_table.setRowCount(0)
        self.main_table.setGeometry(QtCore.QRect(0, 22, self.size_x - 4, self.size_y - 26))
        self.main_table.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.main_table.horizontalHeader().setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        self.main_table.palette().setBrush(QtGui.QPalette.Highlight, QtGui.QBrush(QtGui.QColor('white')))
        self.main_table.palette().setBrush(QtGui.QPalette.HighlightedText, QtGui.QBrush(QtGui.QColor('black')))
        self.main_table.verticalHeader().hide()
        self.main_table.horizontalHeader().hide()
        self.main_table.setShowGrid(False)

        self.main_table.setMouseTracking(True)
        self.main_table.cellEntered.connect(self.cellHover)

        #self.drawFraming()

        self.contents = {}
        self.contents_list = []
        self.current_hover = [0, 0]
        self.del_func = None
		
	## Draws a framing around itself
    #  @param self The object pointer.
    def drawFraming(self):
        self.top_line = QtWidgets.QFrame(self)
        self.top_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.top_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.top_line.setFixedWidth(self.size_x)
        self.top_line.setFixedHeight(4)
        self.top_line.setStyleSheet('QFrame{border: 4px solid;}')

        self.bottom_line = QtWidgets.QFrame(self)
        self.bottom_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.bottom_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.bottom_line.setFixedWidth(self.size_x)
        self.bottom_line.setFixedHeight(4)
        self.bottom_line.setStyleSheet('QFrame{border: 4px solid;}')
        self.bottom_line.move(0, self.size_y - 4)

        self.left_line = QtWidgets.QFrame(self)
        self.left_line.setFrameShape(QtWidgets.QFrame.VLine)
        self.left_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.left_line.setFixedHeight(self.size_y)
        self.left_line.setFixedWidth(4)
        self.left_line.setStyleSheet('QFrame{border: 4px solid;}')
        self.left_line.move(0, 0)

        self.right_line = QtWidgets.QFrame(self)
        self.right_line.setFrameShape(QtWidgets.QFrame.VLine)
        self.right_line.setFrameShadow(QtWidgets.QFrame.Raised)
        self.right_line.setFixedHeight(self.size_y)
        self.right_line.setFixedWidth(4)
        self.right_line.setStyleSheet('QFrame{border: 4px solid;}')
        self.right_line.move(self.size_x - 4, 0)

        self.title_line = QtWidgets.QFrame(self)
        self.title_line.setFrameShape(QtWidgets.QFrame.HLine)
        self.title_line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.title_line.setFixedWidth(self.size_x)
        self.title_line.setFixedHeight(4)
        self.title_line.move(0, 18)
        self.title_line.setStyleSheet('QFrame{border: 4px solid;}')

	## Method to add content to display
    #  @param self The object pointer
	#  @param topic Topic of the JSON parameter to add
	#  @param payload Payload of the JSON parameter to add
    def addContent(self, topic, payload):
        rowPos = self.main_table.rowCount()

        for i in range (0, rowPos):
            if topic in self.main_table.item(i, 0).text():
                return False

        self.contents[topic] = payload
        self.main_table.insertRow(rowPos)

        newItem = QtWidgets.QTableWidgetItem(topic + " : " +  payload, 1)
        newItem.setFlags(QtCore.Qt.NoItemFlags)
        newItem.setForeground((QtGui.QBrush(QtGui.QColor('black'))))
        self.main_table.setItem(rowPos, 0, newItem)
        newItem = QtWidgets.QTableWidgetItem("")
        newItem.setFlags(QtCore.Qt.NoItemFlags)
        self.main_table.setItem(rowPos, 1, newItem)
        newItem = QtWidgets.QTableWidgetItem("")
        newItem.setFlags(QtCore.Qt.NoItemFlags)
        self.main_table.setItem(rowPos, 2, newItem)
        newBtn = QtWidgets.QPushButton(" X ", self.main_table)
        newBtn.setFixedSize(60, 30)
        newBtn.setStyleSheet("QPushButton[enabled=\"false\"]"
                             "{ color: white }"
                             "QPushButton"
                             "{ color: red;"
                             "  font-weight: bold; "
                             "  background-color: white; }")
        newBtn.setToolTip("Delete content")
        newBtn.setFlat(True)
        newBtn.clicked.connect(lambda: self.delTopic(topic))
        self.main_table.setCellWidget(rowPos, 1, newBtn)
        #TODO: Find solution for buttons not initially hiding
        self.main_table.cellWidget(rowPos, 1).hide()
	
	## Method to delete content in display
    #  @param self The object pointer
	#  @param topic Topic of the JSON parameter delete
    def delTopic(self, topic):
        self.delAction(topic)
        for i in range (0, self.main_table.rowCount()):
            if topic in self.main_table.item(i, 0).text():
                self.main_table.removeRow(i)
                self.cellHover(i, 0)
                return

    ## Setter for the deleting function to associate with the class
	#  @param self The object pointer
	#  @param new_func PCallback function to set to deleting event
    def setDelFunc(self, new_func):
        self.del_func = new_func

	## Method to delete a particular action
	#  @param self The object pointer
	#  @param topic Topic of the action to target
    def delAction(self, topic):
        del self.contents[topic]
        if self.del_func is not None:
            self.del_func(topic)
	
	## Method to animate a cell hovering effect
	#  @param self The object pointer
	#  @param row Hovered row
	#  @param column Hovered column
    def cellHover(self, row, column):
        if self.main_table.item(self.current_hover[0], 0) is None:
            return
        if self.current_hover != [row, column]:
            self.main_table.item(self.current_hover[0], 0).setBackground(QtGui.QBrush(QtGui.QColor('white')))
            #TODO: Put back hiding when figured out proper way to hide buttons on appearance
            self.main_table.cellWidget(self.current_hover[0], 1).hide()
            self.main_table.item(self.current_hover[0], 1).setBackground(QtGui.QBrush(QtGui.QColor('white')))
        self.main_table.item(row, 0).setBackground(QtGui.QBrush(QtGui.QColor('lightgrey')))
        self.main_table.cellWidget(row, 1).show()
        self.main_table.item(row, 1).setBackground(QtGui.QBrush(QtGui.QColor('lightgrey')))
        self.current_hover = [row, column]
		
## The Ui_ComportWindow class
#  @brief Widget meant to artifically send a communication port packet
class Ui_ComportWindow(QtWidgets.QWidget):
	
	## Confirmation if the window has already been launched
    __launched = False

    ## The class' initialisation method
    #  @param self The object pointer
    def __init__(self):
        super().__init__()
        self.mem_data = GlobalMemory()
        self.topic_field = None
        self.content_table = None
        self.pl_topic_field = None
        self.pl_pl_field = None
        self.add_content_btn = None
        self.full_message_view = None
        self.send_btn = None
		
    ## Setup for the topic field
    #  @param self The object pointer
	#  @param window Window in which to setup the topic field
    def setupTopic(self, window):
        window.topic_field_lbl = QtWidgets.QLabel(window)
        window.topic_field_lbl.setText("Main topic: ")
        window.topic_field_lbl.move(10, 15)

        window.topic_field = QtWidgets.QLineEdit(window)
        window.topic_field.setMaxLength(4)
        window.topic_field.resize(300, 30)
        window.topic_field.move(120, 10)
        window.topic_field.textChanged.connect(lambda : window.updateMessageView(window))
	
	## Setup for the contents table field
    #  @param self The object pointer
	#  @param window Window in which to setup the contents table field
    def setupContentsTable(self, window):
        window.content_table = ContentsTable(window, 410, 300, "Message's payload: ")
        window.content_table.move(10, 50)
        window.content_table.del_func = (lambda x: window.updateMessageView(window))

	## Setup fo the contents adding field
    #  @param self The object pointer
	#  @param window Window in which to setup the contents adding field
    def setupContentsAdding(self, window):
        window.pl_topic_field_lbl = QtWidgets.QLabel(window)
        window.pl_topic_field_lbl.setText("Sub-topic: ")
        window.pl_topic_field_lbl.move(10, 370)

        window.pl_topic_field = QtWidgets.QLineEdit(window)
        window.pl_topic_field.resize(300, 30)
        window.pl_topic_field.move(120, 365)

        window.pl_pl_field_lbl = QtWidgets.QLabel(window)
        window.pl_pl_field_lbl.setText("Payload: ")
        window.pl_pl_field_lbl.move(10, 410)

        window.pl_pl_field = QtWidgets.QLineEdit(window)
        window.pl_pl_field.resize(300, 30)
        window.pl_pl_field.move(120, 405)

        window.add_content_btn = QtWidgets.QPushButton(window)
        window.add_content_btn.setText("Add")
        window.add_content_btn.setToolTip("Add sub-topic")
        window.add_content_btn.move(340, 450)
        window.add_content_btn.clicked.connect(lambda: window.addContents(window))

	## Setup various sending utilities
    #  @param self The object pointer
	#  @param window Window in which to setup the utilities field
    def setupSendingUtilities(self, window):
        window.full_message_view = QtWidgets.QPlainTextEdit(window)
        window.full_message_view.setReadOnly(True)
        window.full_message_view.resize(410, 50)
        window.full_message_view.move(10, 485)
        window.full_message_view.setStyleSheet("QPlainTextEdit { color: green; background-color: lightgrey; }")

        window.send_btn = QtWidgets.QPushButton(window)
        window.send_btn.setText("Send")
        window.send_btn.setToolTip("Send this message through com port")
        window.send_btn.move(340, 550)
        window.send_btn.clicked.connect(lambda: window.sendData(window))

	## Retranslate the window's contents if necessary
    #  @param self The object pointer
	#  @param window Window in which to retranslate the contents
    def retranslateUi(self, window):
        _translate = QtCore.QCoreApplication.translate
        window.setWindowTitle(_translate("ComportWindow", "Operator - Communication port tool"))

	## Setup fo the contents of the whole window
    #  @param self The object pointer
	#  @param window Window in which to setup the contents
    def setupUi(self, window):
        window.setObjectName("ComportWindow")
        #TODO: Need to create an error case where file is unavailable
        window.setWindowIcon(QtGui.QIcon("Mercury.png"))
        window.setFixedHeight(600)
        window.setFixedWidth(450)

        window.setupTopic(window)
        window.setupContentsTable(window)
        window.setupContentsAdding(window)
        window.setupSendingUtilities(window)

        window.retranslateUi(window)
	
	## Method for launching the window
    #  @param self The object pointer
    def launch(self):
        if self.__launched is False:
            self.__launched = True
            self.setupUi(self)
            self.show()
			
	## Method to accept a close event
    #  @param self The object pointer
	#  @param event The event to close
    def closeEvent(self, event):
        self.__launched = False
        event.accept()

	## Method add contents to the output
    #  @param self The object pointer
	#  @param window Window in which to setup the contents
    def addContents(self, window):
        window.content_table.addContent(window.pl_topic_field.text(), window.pl_pl_field.text())
        window.pl_pl_field.setText("")
        window.pl_topic_field.setText("")
        window.updateMessageView(window)

	## Method to update the message view
    #  @param self The object pointer
	#  @param window Window in which to update the message view
    def updateMessageView(self, window):
        window.full_message_view.setPlainText(window.getMessage(window))

	## Getter for the currently resulting message view
    #  @param self The object pointer
	#  @param window Window from which to get the current message
    def getMessage(self, window):
        message_dict = {}
        message_dict['params'] = window.content_table.contents
        message_dict['func'] = window.topic_field.text()
        return ("{\"func\":\"" + message_dict['func'] + "\",\"params\":" + json.dumps(message_dict['params']) + "}")

	## Method to send over serial port the currently set data_exist
    #  @param self The object pointer
	#  @param window Window from which to transfer the data packet
    def sendData(self, window):
        if window.mem_data.data_exist('action', 'send_com_str'):
            window.mem_data.get_data('action', 'send_com_str')((window.full_message_view.toPlainText().strip()))
