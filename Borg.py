## @package pyexample
#  Borg.py
#
#  @brief File containing the Borg class pattern
#  @author Alex Martelli
#  @date   April 2nd, 2018
#  @version 1.0.0

## The Borg class
#  @brief Alternative to a Singleton, forces all its instances to share the same state
class Borg:
    ## The shared state object
    __shared_state = {}

    ## The class' initialisation method
    #  @param self The object pointer
    def __init__(self):
        self.__dict__ = self.__shared_state
