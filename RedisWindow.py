## @package pyexample
#  RedisWindow.py
#
#  @brief File containing the Ui_RedisWindow class pattern
#  @author William Marcoux
#  @date   April 22nd, 2018
#  @version 1.0.0

import redis
from PyQt5 import QtCore, QtGui, QtWidgets
from GlobalMemory import GlobalMemory


## The Ui_RedisWindow class
#  @brief The Ui_RedisWindow a graphical window allowing for peeks into the redis database
class Ui_RedisWindow(QtWidgets.QWidget):

	## Confirmation if the window has already been launched
    __launched = False

	## The class' initialisation method
    #  @param self The object pointer
	#  @param host Host name of the redis database
	#  @param port Port of the redis database
	#  @param db database ID of the redis database
    def __init__(self, host='localhost', port=6379, db=0):
        super().__init__()

        self.host_field = None
        self.use_local_chkbx = None
        self.port_field = None
        self.db_field = None
        self.subject_field = None
        self.content_field = None
        self.request_mode_chkbx = None
        self.result_view = None
        self.send_btn = None

        self.host = host
        self.port = port
        self.db = db
        self.conn = redis.Redis(connection_pool=redis.ConnectionPool(host=self.host, port=self.port, db=self.db))

	## Setup for the contact-related fields
    #  @param self The object pointer
	#  @param window Window in which to setup
    def setupContactFields(self, window):
        window.host_field_lbl = QtWidgets.QLabel(window)
        window.host_field_lbl.setText("Host address: ")
        window.host_field_lbl.move(10, 15)

        window.host_field = QtWidgets.QLineEdit(window)
        window.host_field.resize(300, 30)
        window.host_field.setText(window.host)
        window.host_field.move(120, 10)

        window.port_field_lbl = QtWidgets.QLabel(window)
        window.port_field_lbl.setText("Port: ")
        window.port_field_lbl.move(10, 60)

        window.port_field = QtWidgets.QLineEdit(window)
        window.port_field.resize(300, 30)
        window.port_field.setValidator(QtGui.QIntValidator())
        window.port_field.setText(str(window.port))
        window.port_field.move(120, 55)

        window.db_field_lbl = QtWidgets.QLabel(window)
        window.db_field_lbl.setText("Database ID: ")
        window.db_field_lbl.move(10, 105)

        window.db_field = QtWidgets.QLineEdit(window)
        window.db_field.resize(300, 30)
        window.db_field.setValidator(QtGui.QIntValidator())
        window.db_field.setText(str(window.db))
        window.db_field.move(120, 100)

        window.use_local_chkbx_lbl = QtWidgets.QLabel(window)
        window.use_local_chkbx_lbl.setText("Use local host: ")
        window.use_local_chkbx_lbl.move(190, 357)

        window.use_local_chkbx = QtWidgets.QCheckBox(window)
        window.use_local_chkbx.setToolTip("Check to use local host")
        window.use_local_chkbx.move(300, 360)
        window.use_local_chkbx.clicked.connect(lambda : window.useLocalhostSetup(window))

	## Setup for the request-related fields
    #  @param self The object pointer
	#  @param window Window in which to setup
    def setupRequestFields(self, window):
        window.subject_field_lbl = QtWidgets.QLabel(window)
        window.subject_field_lbl.setText("Subject: ")
        window.subject_field_lbl.move(10, 165)

        window.subject_field = QtWidgets.QLineEdit(window)
        window.subject_field.resize(300, 30)
        window.subject_field.move(120, 160)

        window.content_field_lbl = QtWidgets.QLabel(window)
        window.content_field_lbl.setText("Content: ")
        window.content_field_lbl.move(10, 210)

        window.content_field = QtWidgets.QLineEdit(window)
        window.content_field.resize(300, 30)
        window.content_field.move(120, 205)

        window.request_mode_chkbx_lbl = QtWidgets.QLabel(window)
        window.request_mode_chkbx_lbl.setText("Request information: ")
        window.request_mode_chkbx_lbl.move(10, 357)

        window.request_mode_chkbx = QtWidgets.QCheckBox(window)
        window.request_mode_chkbx.setToolTip("Check to make an information request")
        window.request_mode_chkbx.move(160, 360)
        window.request_mode_chkbx.clicked.connect(lambda : window.setRequestMode(window))

	## Setup for the sending utilities
    #  @param self The object pointer
	#  @param window Window in which to setup
    def setupSendUtilities(self, window):
        window.result_view = QtWidgets.QPlainTextEdit(window)
        window.result_view.setReadOnly(True)
        window.result_view.resize(410, 50)
        window.result_view.move(10, 250)
        window.result_view.setStyleSheet("QPlainTextEdit { color: green; background-color: lightgrey; }")
        window.result_view.hide()

        window.send_btn = QtWidgets.QPushButton(window)
        window.send_btn.setText("Send")
        window.send_btn.setToolTip("Send request to Redis database")
        window.send_btn.move(340, 355)
        window.send_btn.clicked.connect(lambda: window.sendData(window))
	
	## Retranslate the window's contents if necessary
    #  @param self The object pointer
	#  @param window Window in which to retranslate the contents
    def retranslateUi(self, window):
        _translate = QtCore.QCoreApplication.translate
        window.setWindowTitle(_translate("RedisWindow", "Operator - Redis database tool"))

	## Setup fo the contents of the whole window
    #  @param self The object pointer
	#  @param window Window in which to setup the contents
    def setupUi(self, window):
        window.setObjectName("RedisWindow")
        # TODO: Need to create an error case where file is unavailable
        window.setWindowIcon(QtGui.QIcon("Mercury.png"))
        window.setFixedHeight(400)
        window.setFixedWidth(450)

        window.setupRequestFields(window)
        window.setupContactFields(window)
        window.setupSendUtilities(window)

        window.retranslateUi(window)

	## Method for launching the window
    #  @param self The object pointer
    def launch(self):
        if self.__launched is False:
            self.__launched = True
            self.setupUi(self)
            self.show()
			
	## Closing event for the window
    #  @param self The object pointer
    def closeEvent(self, event):
        self.__launched = False
        event.accept()
		
	## Method for setting the Redis database contact information within a localhost context
    #  @param self The object pointer
    def useLocalhostSetup(self, window):
        if window.use_local_chkbx.isChecked():
            window.host_field.setDisabled(True)
            window.host_field.setText("localhost")
            window.host_field.setStyleSheet("color: rgb(77, 77, 77);")
        else:
            window.host_field.setDisabled(False)
            window.host_field.setStyleSheet("color: black;")

	## Method for setting the request mode in the GUI (for reading or sending to db)
    #  @param self The object pointer
    def setRequestMode(self, window):
        if window.request_mode_chkbx.isChecked():
            window.content_field.setDisabled(True)
            window.result_view.show()
        else:
            window.content_field.setDisabled(False)
            window.result_view.hide()

	## Method for updating the connexion with the current parameters
    #  @param self The object pointer
    def updateConn(self, window):
        window.host = window.host_field.text()
        window.port = int(window.port_field.text())
        window.db = int(window.db_field.text())
        window.conn = redis.Redis(connection_pool=redis.ConnectionPool(host=self.host, port=self.port, db=self.db))

	## Method for sending data to the Redis database
    #  @param self The object pointer
    def sendData(self, window):
        window.updateConn(window)
        if window.request_mode_chkbx.isChecked():
            window.result_view.setPlainText(str(window.conn.get(window.subject_field.text())))
        else:
            window.conn.set(window.subject_field.text(), window.content_field.text())
