::@file pyQtCompiler.bat
::@brief Transforms qt ui files into py files
::@author William Marcoux (mailto: william.marcoux.98@gmail.com)
::@version 1.0.0
::@date April 1st, 2018

::This file is just meant to simplify the compiling
::pyQt related files.


@ECHO off &SETLOCAL

ECHO.

WHERE python
IF %ERRORLEVEL% NEQ 0 GOTO error_not_installed_python

WHERE pyuic5
IF %ERRORLEVEL% NEQ 0 GOTO error_not_installed_pyuic

   
FOR %%f IN (%*) DO (
	CALL SET "MyVar= %%MyVar%% %%~nf.ui"
)
 
SET filesList=%MyVar:~2%
SET /p outputFile=Type the name of the output file:

ECHO %filesList%

pyuic5 -o %outputFile% %filesList%

ECHO.
ECHO Files created.

GOTO end

:error_not_installed_python
	
ECHO python cannot be found on your system.
GOTO end

:error_not_installed_pyuic

ECHO pyuic5 cannot be found on your system.

:end

ECHO.
PAUSE