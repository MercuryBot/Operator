## @package pyexample
#  GlobalMemory.py
#
#  @brief File containing the GlobalMemory class pattern
#  @author William Marcoux
#  @date   April 2nd, 2018
#  @version 1.0.0

from TMutex import TMutex
from Borg import Borg
import copy
SUCCESS = True
FAILED = False


## The GlobalMemory class
#  @brief Holds and protects the application's global memory
class GlobalMemory():

	## Data dictionnary
    __data_dict = {}
	## Mutex to protect data dictionnary
    __mutex = TMutex()

	## The class' initialisation method
    #  @param self The object pointer
    def __init__(self):
        self.__data_dict = {}
        self.__mutex = TMutex()

	## Method to check if a set of argument exists
	#  @param self The object pointer
	#  @param arg1 Main argument to verify
	#  @param arg2 Sub-argument to verify
	#  @return True if the data is contained, False if it isn't
    def data_exist(self, arg1, arg2):
        if arg1 in self.__data_dict:
            if arg2 in self.__data_dict[arg1]:
                return True
            else:
                return False
        else:
            return False
			
	## Method to add data to the data dictionnary
	#  @param self The object pointer
	#  @param arg Name of the argument to add
	#  @param value Value of the argument to add
    def add_data(self, arg, value):
        self.__data_dict[arg] = value
        self.__mutex.add_lock(arg)

	## Method to remove data from the data dictionnary
	#  @param self The object pointer
	#  @param *args List of arguments to retire
    def remove_data(self, *args):
        self.__mutex.lock(args)
        for i in args:
            if i in self.__data_dict:
                del self.__data_dict[i]
        self.__mutex.remove_lock(args)

	## Getter for a set of arguments from the data dictionnary
	#  @param self The object pointer
	#  @param arg1 Main argument to get
	#  @param arg2 Sub-argument to get
	#  @return The gotten data if it exists, FAILED keyword value if it doesn't
    def get_data(self, arg1, arg2):
        if self.data_exist(arg1, arg2) is False:
            return FAILED
        else:
             return self.__data_dict[arg1][arg2]

	## Setter for a set of arguments from the data dictionnary
	#  @param self The object pointer
	#  @param *args All arguments to target
	#  @param value Value to set these arguments
    def set_data(self, *args, value):
        tmp_data = self.__data_dict
        if self.data_exist(args) is False:
            return FAILED
        else:
            self.__mutex.lock(args[0])
            for i in args:
                tmp_data = tmp_data[i]
            self.__data_dict[tmp_data] = value
            self.__mutex.unlock(args[0])
            return SUCCESS