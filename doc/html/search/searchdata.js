var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstu",
  1: "bcdefgorstu",
  2: "ep",
  3: "_bcglmrst",
  4: "_acdeghiklmpqrstu",
  5: "abcdefhiklmnprstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables"
};

