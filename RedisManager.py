## @package pyexample
#  RedisManager.py
#
#  @brief File containing the RedisReceptorsDbManager and RedisUnitsDbManager class patterns
#  @author William Marcoux
#  @date   April 22nd, 2018
#  @version 1.0.0

import redis
import json

possible_ids = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + '0123456789' + 'abcdefghijklmnopqrstuvwxyz'

## The RedisReceptorsDbManager class
#  @brief Manages the redis database associated with connected Receptors
class RedisReceptorsDbManager:
		
	## The class' initialisation method
    #  @param self The object pointer
	#  @param host Host name of the redis database
	#  @param port Port of the redis database
	#  @param db database ID of the redis database
    def __init__(self, host="localhost", port=6379, db=1):
        self.host = host
        self.port = port
        self.db = db
        self.receptor_db = redis.Redis(connection_pool=redis.ConnectionPool(host=host, port=self.port, db=self.db))
        self.current_id = None

	## Method to initialize connection with the redis database
    #  @param self The object pointer
	#  @param host Host name of the redis database
	#  @param port Port of the redis database
	#  @param db database ID of the redis database
    def connect(self, host="localhost", port=6379, db=1):
        self.host = host
        self.port = port
        self.db = db
        self.receptor_db = redis.Redis(connection_pool=redis.ConnectionPool(host=host, port=self.port, db=self.db))

	## Method to get the next available communicatino ID for a receptor
    #  @param self The object pointer
	#  @return The next available communicatino ID for a receptor
    def get_next_free_receptor_id(self):
        for i in range(0, len(possible_ids)):
            if self.receptor_db.exists(possible_ids[i]):
                continue
            else:
                return possible_ids[i]
        return 'z'

	## Method to add a receptor tp the database
    #  @param self The object pointer
	#  @param receptor_id ID of the new receptor
    def add_receptor(self, receptor_id):
        self.receptor_db.set(receptor_id, "")

	## Method to add a associated units to a receptor
    #  @param self The object pointer
	#  @param receptor_id ID of the receptor
	#  @param units List of units ID to set
    def add_receptor_units(self, receptor_id, units=[]):
        units_list = list(self.receptor_db.get(receptor_id))
        units_list.append(units)
        self.receptor_db.set(receptor_id, str(units_list))

	## Method to delete a receptor from the database
    #  @param self The object pointer
	#  @param receptor_id ID of the receptor to delete
    def delete_receptor(self, receptor_id):
        receptor_id.delete(receptor_id)


## The RedisUnitsDbManager class
#  @brief Manages the redis database associated with connected Units
class RedisUnitsDbManager:
		
	## The class' initialisation method
    #  @param self The object pointer
	#  @param host Host name of the redis database
	#  @param port Port of the redis database
	#  @param db database ID of the redis database
    def __init__(self, host="localhost", port=6379, db=0):
        self.host = host
        self.port = port
        self.db = db
        self.units_db = redis.Redis(connection_pool=redis.ConnectionPool(host=host, port=self.port, db=self.db))
        self.receptor_manager = RedisReceptorsDbManager()

	## Method to initialize connection with the redis database
    #  @param self The object pointer
	#  @param host Host name of the redis database
	#  @param port Port of the redis database
	#  @param db database ID of the redis database
    def connect(self, host="localhost", port=6379, db=0):
        self.host = host
        self.port = port
        self.db = db
        self.units_db = redis.Redis(connection_pool=redis.ConnectionPool(host=host, port=self.port, db=self.db))

	## Method to get a unit from the Redis database
    #  @param self The object pointer
	#  @param unit_address Address of the unit to get
	#  @return Unit corresponding to the address
    def get_unit(self, unit_address):
        if self.receptor_manager.current_id is not None:
            return self.units_db.get(unit_address)
			
	## Method to add a unit to the Redis database
    #  @param self The object pointer
	#  @param unit_address Address of the unit to add
    def add_unit(self, unit_address):
        if self.receptor_manager.current_id is not None:
            self.units_db.set(unit_address, "{'position' : 'unknown'}")
		
	## Method to modify a unit in the Redis database
    #  @param self The object pointer
	#  @param unit_address Address of the unit to modify
	#  @param params Parameters of the unit to set
    def modify_unit(self, unit_address, params):
        if self.receptor_manager.current_id is not None:
            self.units_db.set(unit_address, json.dumps(params))

	## Method to remove a unit from the Redis database
    #  @param self The object pointer
	#  @param unit_address Address of the unit to remove
    def delete_unit(self, unit_address):
        if self.receptor_manager.current_id is not None:
            self.units_db.delete(unit_address)
