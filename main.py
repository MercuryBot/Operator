## @package pyexample
#  main.py
#
#  @brief File containing the program's main routine
#  @author William Marcoux
#  @date   April 2nd, 2018
#  @version 1.0.0

from MainWindow import Ui_MainWindow
from TelecomWindow import Ui_TelecomWindow
from ComportWindow import Ui_ComportWindow
from RedisWindow import Ui_RedisWindow
from ControlWindow import Ui_ControlWindow
from GlobalMemory import GlobalMemory
import sys
from RedisManager import RedisUnitsDbManager
from SerialCom import SerialCom
import serial.tools.list_ports
import TimedSleep
import redis

#import mysql.connector as mariadb
from PyQt5 import QtGui, QtWidgets, QtCore

REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_UNITS_DB = 0

SQL_DEFAULT_USER = 'Operator'
SQL_DEFAULT_PASSWORD = 'Operator'
SQL_ITEMS_DB = 'Inventory'
SQL_COMMANDS_DB = 'Commands'

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    main_memory = GlobalMemory()
    main_window = Ui_MainWindow()
    main_window.mem_data = main_memory
    telecom_window = Ui_TelecomWindow()
    telecom_window.mem_data = main_memory
    comport_window = Ui_ComportWindow()
    comport_window.mem_data = main_memory
    redis_window = Ui_RedisWindow()
    redis_window.mem_data = main_memory
    control_window = Ui_ControlWindow()
    control_window.mem_data = main_memory
    redis_manager = RedisUnitsDbManager()

    main_memory.add_data('ui' , {'main' : main_window, 'telecom' : telecom_window, 'redis' : redis_window, 'comport' : comport_window, 'ctrl' : control_window})
    main_memory.add_data('action', {'com_list' : main_window.serial.sweep_for_receptor, 'connect_com' : main_window.serial.connectPort, 'send_com_packet' : main_window.serial.send_packet, 'send_com_str' : main_window.serial.send_str})
    main_memory.add_data('operation', {'stop_all' : lambda: main_window.serial.send_packet("STOP", {})})

    main_window.serial.redis_db = redis_manager
    main_window.launch()
    control_window.launch("AAAAA")

    sys.exit(app.exec_())
