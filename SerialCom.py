## @package pyexample
#  SerialCom.py
#
#  @brief File containing the Serialcom class patterns
#  @author William Marcoux
#  @date   April 16th, 2018
#  @version 1.0.0

from TThread import  TThread
from TimedSleep import *
import serial
import sys
import glob
import json
import time
import timeout_decorator
from GlobalMemory import GlobalMemory
from PyQt5 import QtCore, QtGui, QtWidgets
from collections import OrderedDict

DEFAULT_BAUDRATE = 9600

## The SerialSignals class
#  @brief Allows the thread to send signals to the main thread in the PyQt5 way
class SerialSignals(QtCore.QObject):
    packetIn = QtCore.pyqtSignal(str)

## The SerialCom class
#  @brief Main serial communication thread
class SerialCom(TThread):

	## The class' initialisation method
    #  @param self The object pointer
	#  @param thread_id Thread ID to set
	#  @param name Name of the thread
	#  @param sleep_time_us Sleep delay on task's periodic routine
    def __init__(self, thread_id=0, name="lambda", sleep_time_us=1):
        super().__init__(thread_id, name, sleep_time_us)
        self.com_port = ""
        self.port = serial.Serial()
        self.mem_data = GlobalMemory()
        self.signals = SerialSignals()
        self.com_str = ""
        self.paused = False

	## Method for the main operating task
    #  @param self The object pointer
    def task(self):
        if self.port.is_open and self.paused is False:
            try:
                line = self.port.read()
            except:
                line = ""
            if line is not "":
                self.recept_packet(str(line, "utf-8"))
				
	## Method for stopping the thread activity
    #  @param self The object pointer
    def stop(self):
        self.port.close()
        TThread.stop()

	## Method for sweeping active com ports
    #  @param self The object pointer
	#  @return List of active com ports
    def sweep_com_ports(self):
        comlist = serial.tools.list_ports.comports()
        connected = []
        for element in comlist:
            connected.append(element.device)
        return connected

		
	## Method for sweeping active com ports connected to a receptor
    #  @param self The object pointer
	#  @return List of active com ports connected to a receptor
    def sweep_for_receptor(self):
        return_list = []
        port_sweep = serial.Serial()
        for i in self.sweep_com_ports():
            port_sweep.baudrate = DEFAULT_BAUDRATE
            port_sweep.port = str(i)
            port_sweep.open()
            sleep(1)
            line = str(self.read(20, port_sweep))
            print(line)
            if "INIT" in line:
                return_list.append(i)
                port_sweep.close()
        return return_list

	## Method for connecting to a serial port
    #  @param self The object pointer
	#  @param baudrate Baudrate value to set
	#  @param port Port name
    def connectPort(self, baudrate=DEFAULT_BAUDRATE, port=""):
        self.port.close()
        self.port.baudrate = baudrate
        self.port.port = port
        self.port.open()

	## Method for reading from serial port
    #  @param self The object pointer
	#  @param num Number of bytes to read
	#  @param target_port Port to read from
    @timeout_decorator.timeout(5)
    def read(self, num, target_port):
        return target_port.read(num)

	## Method for reception of a serial packet
    #  @param self The object pointer
	#  @param packet_str String of serial packet to recept
    def recept_packet(self, packet_str):
        self.com_str += packet_str

        if "{" in self.com_str and "}}" in self.com_str:
            self.signals.packetIn.emit(self.com_str)
            self.interpret_packet(self.com_str)
            self.com_str = ""

	## Method for sending a serial packet
    #  @param self The object pointer
	#  @param topic Main topic of the packet
	#  @param params Parameters to add to the packet
    def send_packet(self, topic, params):
        if self.port.is_open is False or self.paused is True:
            return
        self.port.write(("{\"func\":\"" + topic + "\",\"params\":" + json.dumps(params) + "}").encode())
        self.signals.packetIn.emit("\n>>> "  + ("{\"func\":\"" + topic + "\",\"params\":" + json.dumps(params) + "}"))

	## Sends a string over serial port
    #  @param self The object pointer
	#  @param str String to send
    def send_str(self, str):
        if self.port.is_open is False or ("{" not in str and "}}" not in str) or self.paused is True:
            return
        self.port.write(str.encode())
        self.signals.packetIn.emit("\n>>> " + str)

	## Pause the serial communication
    #  @param self The object pointer
	#  @param param Parameter to set the paused member to
    def set_paused(self, param):
        self.paused = param

	## Method for interpreting a serial packet
    #  @param self The object pointer
	#  @param packet_str String of serial packet to interpret
    def interpret_packet(self, packet_str):
        packet_dict = json.loads(packet_str)

        if "INIT" in packet_dict['func']:
            if self.redis_db is not None:
                self.redis_db.receptor_manager.current_id = self.redis_db.receptor_manager.get_next_free_receptor_id()
                self.send_packet("INIT", {"ID" :self.redis_db.receptor_manager.current_id})
        elif "NCON" in packet_dict['func']:
            if self.redis_db is not None:
                self.redis_db.add_unit(packet_dict['params']['address'])
        elif "CPOS" in packet_dict['func']:
            new_details = json.loads(self.redis_db.get_unit(packet_dict['params']['address']))
            new_details['position'] = packet_dict['code_1'] + packet_dict['code_2'] + packet_dict['code_3']
            self.redis_db.modify_unit(packet_dict['params']['address'], json.dumps(new_details))
        elif "ABAT" in packet_dict['func']:
            new_details = json.loads(self.redis_db.get_unit(packet_dict['params']['address']))
            new_details['alert_battery'] = "yes"
            self.redis_db.modify_unit(packet_dict['params']['address'], json.dumps(new_details))
        elif "LBAT" in packet_dict['func']:
            new_details = json.loads(self.redis_db.get_unit(packet_dict['params']['address']))
            new_details['level_battery'] = packet_dict['params']['level']
            self.redis_db.modify_unit(packet_dict['params']['address'], json.dumps(new_details))
        elif "ALRT" in packet_dict['func']:
            new_details = json.loads(self.redis_db.get_unit(packet_dict['params']['address']))
            new_details['alert_code'] = packet_dict['params']['code']
            self.redis_db.modify_unit(packet_dict['params']['address'], json.dumps(new_details))
        elif "CSTT" in packet_dict['func']:
            new_details = json.loads(self.redis_db.get_unit(packet_dict['params']['address']))
            new_details['current_state'] = packet_dict['params']['data']
            self.redis_db.modify_unit(packet_dict['params']['address'], json.dumps(new_details))
        elif "SDON" in packet_dict['func']:
            new_details = json.loads(self.redis_db.get_unit(packet_dict['params']['address']))
            new_details['sequ_' + packet_dict['params']['code']] = "Done"
            self.redis_db.modify_unit(packet_dict['params']['address'], json.dumps(new_details))
        elif "PPKT" in packet_dict['func']:
            if self.mem_data.data_exist('ui', 'main'):
                self.mem_data.get_data('ui', 'main').packet_monitor.inputText(packet_dict['params']['direction'] + ": " + packet_dict['params']['packet'])


