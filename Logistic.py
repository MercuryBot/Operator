## @package pyexample
#  Logistic.py
#
#  @brief File containing the Logistic class pattern
#  @author William Marcoux
#  @date   April 28th, 2018
#  @version 1.0.0

from TThread import  TThread
from TimedSleep import *
import serial
import sys
import glob
import json
import time
import timeout_decorator
from GlobalMemory import GlobalMemory
from PyQt5 import QtCore, QtGui, QtWidgets
from collections import OrderedDict


## The LogisticSignals class
#  @brief Signals to notify a PyQt processus of a change
class LogisticSignals(QtCore.QObject):
    pass
	
## The Logistic class
#  @brief Task to execute auto-path assignment/finding
class Logistic(TThread):

	## The class' initialisation method
    #  @param self The object pointer
	#  @param thread_id Thread ID to set
	#  @param name Name of the thread
	#  @param sleep_time_us Sleep delay on task's periodic routine
    def __init__(self, thread_id=0, name="lambda", sleep_time_us=1):
        super().__init__(thread_id, name, sleep_time_us)
        self.mem_data = GlobalMemory()
        self.signals = LogisticSignals()
        self.paused = False
        self.map = {}

	## Method for the main operating task
    #  @param self The object pointer
    def task(self):
        pass

	## Method for stopping the thread activity
    #  @param self The object pointer
    def stop(self):
        self.port.close()
        TThread.stop()
		
	## Method to import the warehouse map
    #  @param self The object pointer
	#  @param filename Relative path to the file to get the map from
    def import_map(self, filename):
        try :
            self.map = json.load(open(filename))
        except:
            pass #TODO: Generate can't open file protocol

	## Method for the main path-finding algorithm (contents temporarily removed because of bugs)
    #  @param self The object pointer
	#  @param start_pos Starting position of the unit
	#  @param target_pos Target position of the unit
	#  @param map Map of the warehouse
	#  @param stop_pos Position to interrupt the path finding
    def find_path(self, start_pos, target_pos, map, stop_pos):
		pass




